# 如何构建
1、建立ros2工作空间
> mkdir ~/foc_ws/src -p

2、拷贝ros2目录下的源码包到工作空间
> copy  ros2/*  ~/foc_ws/src -rf 

3、编译串口包
> cd serial-ros2/
> mkdir build && cd build
> cmake ..
> make && sudo make install

3、编译相关的工作环境
> cd ~/foc_ws
> colcon build 


# 如何使用 
1、首先接好开关按钮，或者信号触发上电驱动板，并接好电机线与控制串口

2、初始化ROS2环境

> source ~/foc_ws/install/setup.bash 

3、启动驱动包

>  ros2 launch rikibot_foc_driver rikibot_foc_driver.launch.py

4、启动键盘控制

> ros2 run teleop_twist_keyboard teleop_twist_keyboard

5、查看相关的话题,驱动包产生了battery（驱动板电量）与raw_vel（电机速度）

> ros2 topic list
