//#include <chrono>
//#include <memory>

//#include "rclcpp/rclcpp.hpp"
//#include "std_msgs/msg/string.hpp"
#include "rikibot_foc_driver/rikibot_foc_driver.h"


using namespace std::chrono_literals;


RikibotFocDriver::RikibotFocDriver(): Node("rikibot_foc_driver")
{
  wheel_diameter_ = WHEEL_DIAMETER;
  wheel_circumference_ = PI * wheel_diameter_;
  max_rpm_ = MAX_RPM;
  wheels_x_distance_ = FR_WHEELS_DISTANCE;
  wheels_y_distance_ = LR_WHEELS_DISTANCE;
  now_ = now();

  declare_parameter("port_name", std::string("/dev/ttyTHS1"));
  get_parameter("port_name", port_name_);
  declare_parameter("baud_rate", 115200);
  get_parameter("baud_rate", baud_rate_);

  raw_vel_pub_ = this->create_publisher<riki_msgs::msg::Velocities>("raw_vel", 10);
  battery_pub_ = this->create_publisher<riki_msgs::msg::Battery>("battery", 10);

  topic_timer_ = this->create_wall_timer(10ms, std::bind(&RikibotFocDriver::RosNodeTopicCallback, this));

  velocity_sub_ = this->create_subscription<geometry_msgs::msg::Twist>("cmd_vel", 10, std::bind(&RikibotFocDriver::cmd_vel_callback, this, std::placeholders::_1));


  /**open serial device**/
  try{
      Robot_Serial.setPort(port_name_);
      Robot_Serial.setBaudrate(baud_rate_);
      serial::Timeout to = serial::Timeout::simpleTimeout(2000);
      Robot_Serial.setTimeout(to);
      Robot_Serial.open();
  }catch (serial::IOException& e){
      RCLCPP_INFO(this->get_logger(), "Rikibot Serial Unable to open port");
  }

  if(Robot_Serial.isOpen()){
      RCLCPP_INFO(this->get_logger(), "Rikibot Serial Port opened");
  }else{
  }

}

RikibotFocDriver::~RikibotFocDriver()
{
  Robot_Serial.close();
}

void RikibotFocDriver::RosNodeTopicCallback()
{
    if (true == ReadFormSerial()){
        PublisherMotorVel();
        publisherBattery();
    }
     //RCLCPP_INFO(this->get_logger(), "Hello from ROS2");
}


bool RikibotFocDriver::ReadFormSerial()
{
    if(Robot_Serial.available()){
        rxdata = Robot_Serial.read(Robot_Serial.available());
        if (rxdata.size()==sizeof(RikibotFeedback)){
            memcpy(&Feedback, rxdata.c_str(), sizeof(RikibotFeedback));
            uint16_t checksum = (uint16_t)(Feedback.start^Feedback.rpmR^Feedback.rpmL^Feedback.batVoltage^Feedback.boardTemp^Feedback.curL_DC^Feedback.curR_DC);
            if((Feedback.start == START_FRAME) && (checksum == Feedback.checksum))
                    return true;
            else
                    return false;
        }else{
            return false;
        }
    }else{
        return false;
    }
}


/*cmd_vel Subscriber的回调函数*/
void RikibotFocDriver::cmd_vel_callback(const geometry_msgs::msg::Twist::SharedPtr twist_msg)
{
    SetVelocity(twist_msg->linear.x, twist_msg->linear.y, twist_msg->angular.z);
}


/*底盘速度发送函数*/
void RikibotFocDriver::SetVelocity(double x, double y, double angular_z)
{
    calculateRPM(x, y, angular_z);
    int16_t mSpeedL = map(req_rpm.motor1, -max_rpm_, max_rpm_, -FOC_MAX_PWM,FOC_MAX_PWM);
    int16_t mSpeedR = map(req_rpm.motor2, -max_rpm_, max_rpm_, -FOC_MAX_PWM,FOC_MAX_PWM);
    Command.start = (uint16_t)START_FRAME;
    Command.mSpeedR = (uint16_t)mSpeedR;
    Command.mSpeedL = (uint16_t)mSpeedL;
    //printf("left rpm: mSpeedL: %d, rrpm: %d\r\n", mSpeedL, mSpeedR);
    Command.checksum = (uint16_t)(Command.start^Command.mSpeedR^Command.mSpeedL);
    Robot_Serial.write((uint8_t *)&Command, sizeof(Command));
}


void RikibotFocDriver::PublisherMotorVel()
{
    getVelocities(Feedback.rpmL, Feedback.rpmR);
    raw_vel_msg.linear_x =  vel.linear_x;
    raw_vel_msg.linear_y  = 0;
    raw_vel_msg.angular_z =  vel.angular_z;
    raw_vel_pub_->publish(raw_vel_msg);
}

void RikibotFocDriver::publisherBattery()
{
    if((now() - now_).seconds() > 1/BATTERY_RATE){
        raw_battery_msg.battery = (float)Feedback.batVoltage/100;
        if (raw_battery_msg.battery != 0){
            battery_pub_->publish(raw_battery_msg);
            now_ = now();
        }
    }
}


void RikibotFocDriver::calculateRPM(float linear_x, float linear_y, float angular_z)
{
    float linear_vel_x_mins;
    float linear_vel_y_mins;
    float angular_vel_z_mins;
    float tangential_vel;
    float x_rpm;
    float y_rpm;
    float tan_rpm;

    //convert m/s to m/min
    linear_vel_x_mins = linear_x * 60;
    linear_vel_y_mins = linear_y * 60;

    //convert rad/s to rad/min
    angular_vel_z_mins = angular_z * 60;

    tangential_vel = angular_vel_z_mins * ((wheels_x_distance_ / 2) + (wheels_y_distance_ / 2));

    x_rpm = linear_vel_x_mins / wheel_circumference_;
    y_rpm = linear_vel_y_mins / wheel_circumference_;
    tan_rpm = tangential_vel / wheel_circumference_;

    req_rpm.motor1 = int16_t(x_rpm - y_rpm - tan_rpm);
    req_rpm.motor1 = constrain(req_rpm.motor1, -max_rpm_, max_rpm_);
    //front-right motor
    req_rpm.motor2 = -int16_t(x_rpm + y_rpm + tan_rpm);
    req_rpm.motor2 = constrain(req_rpm.motor2, -max_rpm_, max_rpm_);

}

void RikibotFocDriver::getVelocities(int rpm1, int rpm2 )
{
    float average_rps_x;
    //float average_rps_y;
    float average_rps_a;

    //convert average revolutions per minute to revolutions per second
    average_rps_x = ((float)(rpm1 - rpm2) / 2) / 60;  // RPM
    vel.linear_x = average_rps_x * wheel_circumference_; // m/s

    //convert average revolutions per minute in y axis to revolutions per second
    //average_rps_y = ((float)(-rpm1 + rpm2) / 2) / 60; // RPM

    vel.linear_y = 0;

    //convert average revolutions per minute to revolutions per second
    average_rps_a = ((float)(rpm1 + rpm2) / 2) / 60;
    vel.angular_z =  -(average_rps_a * wheel_circumference_) / ((wheels_x_distance_ / 2) + (wheels_y_distance_ / 2)); //  rad/s

}


int16_t RikibotFocDriver::map(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  auto driver = std::make_shared<RikibotFocDriver>();
  rclcpp::spin(driver);
  rclcpp::shutdown();
  return 0;
}
