#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include <nav_msgs/msg/odometry.hpp>
#include <geometry_msgs/msg/twist.hpp>
//#include <tf/tf.h>
#include <riki_msgs/msg/velocities.hpp>
#include <geometry_msgs/msg/vector3.hpp>
#include <std_msgs/msg/float32_multi_array.hpp>
#include <riki_msgs/msg/imu.hpp>
#include <riki_msgs/msg/battery.hpp>

#include <string>
#include <vector>
#include <math.h>
#include <serial/serial.h>


#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))

#define BATTERY_RATE 1

#define START_FRAME 0xABCD

#define MAX_RPM   	366
#define FOC_MAX_PWM     1000
#define WHEEL_DIAMETER  0.165

#define LR_WHEELS_DISTANCE 0.33
#define FR_WHEELS_DISTANCE 0.0
#define PI                 3.1415926

typedef struct{
    uint16_t  start;          //0XABCD
    int16_t   rpmR;    	      //右轮转速
    int16_t   rpmL;    	      //左轮转速
    int16_t   batVoltage;     //主板电压
    int16_t   boardTemp;      //主板温度
    int16_t   curL_DC;        //左电机电流
    int16_t   curR_DC;        //右电机电流
    uint16_t  checksum;       //校验和
} RikibotFeedback;

typedef struct{
   uint16_t start;
   int16_t  mSpeedR;
   int16_t  mSpeedL;
   uint16_t checksum;
} RikibotCommand;

typedef struct{
  int motor1;
  int motor2;
}MotorRPM;

typedef struct {
  float linear_x;
  float linear_y;
  float angular_z;
}Velocities;


class RikibotFocDriver: public rclcpp::Node{
    public:
        RikibotFocDriver();
        ~RikibotFocDriver();
        void loop();
    
    private:

	    rclcpp::TimerBase::SharedPtr topic_timer_;
	    rclcpp::Publisher<riki_msgs::msg::Velocities>::SharedPtr raw_vel_pub_;
	    rclcpp::Publisher<riki_msgs::msg::Battery>::SharedPtr battery_pub_;
        rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr velocity_sub_;
        bool ReadFormSerial();

        void cmd_vel_callback(const geometry_msgs::msg::Twist::SharedPtr twist_msg);
        void RosNodeTopicCallback();
        void SetVelocity(double x, double y, double angular_z);

        void calculateRPM(float linear_x, float linear_y, float angular_z);
        void getVelocities(int rpm1, int rpm2);
	    int16_t map(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max);
        void PublisherMotorVel();
        void publisherBattery();

        int max_rpm_;
	    float wheel_diameter_;
	    float wheels_x_distance_;
	    float wheels_y_distance_;
	    float wheel_circumference_;

        serial::Serial Robot_Serial;
	
	    RikibotFeedback Feedback;        
        RikibotCommand  Command;
        Velocities vel;
        MotorRPM req_rpm;

        rclcpp::Time now_;

        riki_msgs::msg::Velocities raw_vel_msg;
        riki_msgs::msg::Battery raw_battery_msg;

        std::string port_name_;
        std::string rxdata;

         
        int baud_rate_;

        int control_rate_;

};


