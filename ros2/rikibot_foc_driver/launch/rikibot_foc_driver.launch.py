import os
import launch
import launch.actions
import launch.substitutions
import launch_ros.actions
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    launch_description = launch.LaunchDescription()
    launch_description.add_action(
        launch_ros.actions.Node(
            package='rikibot_foc_driver', 
            node_executable='rikibot_foc_driver',
            name='rikibot_foc_driver', 
            output='screen',)
    )
    return launch_description
