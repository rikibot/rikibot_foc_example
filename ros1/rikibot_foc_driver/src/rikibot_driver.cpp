#include "../include/rikibot_foc_driver/rikibot_driver.h"

RikibotDriver::RikibotDriver()
{
  wheel_diameter_ = WHEEL_DIAMETER;            
  wheel_circumference_ = PI * wheel_diameter_;  
  max_rpm_ = MAX_RPM;                     
  wheels_x_distance_ = FR_WHEELS_DISTANCE;       
  wheels_y_distance_ = LR_WHEELS_DISTANCE;

  ros::NodeHandle nh_;
  ros::NodeHandle nh_p_("~");

  now_ =  now_ = ros::Time::now();

  nh_p_.param<std::string>("port_name",port_name_,std::string("/dev/ttyUSB0"));
  nh_p_.param<int>("baud_rate",baud_rate_,115200);
  nh_p_.param<int>("control_rate",control_rate_,50);

  raw_vel_pub_    = nh_.advertise<riki_msgs::Velocities>("raw_vel",10);
  battery_pub_ = nh_.advertise<riki_msgs::Battery>("battery",1);
  cmd_sub_     = nh_.subscribe("cmd_vel", 10, &RikibotDriver::cmd_vel_callback, this);


  /**open serial device**/
  try{
      Robot_Serial.setPort(port_name_);
      Robot_Serial.setBaudrate(baud_rate_);
      serial::Timeout to = serial::Timeout::simpleTimeout(2000);
      Robot_Serial.setTimeout(to);
      Robot_Serial.open();
  }catch (serial::IOException& e){
      ROS_ERROR_STREAM("Rikibot Serial Unable to open port ");
  }

  if(Robot_Serial.isOpen()){
      ROS_INFO_STREAM("Rikibot Serial Port opened");
  }else{
  }

}

RikibotDriver::~RikibotDriver()
{
  Robot_Serial.close();
}

/*主循环函数*/
void RikibotDriver::loop()
{
    ros::Rate loop_rate(control_rate_);
    while(ros::ok()){
        ros::spinOnce();
        if (true == ReadFormSerial()){
            PublisherMotorVel();
            publisherBattery();
        }
        loop_rate.sleep();
    }

}

/*cmd_vel Subscriber的回调函数*/
void RikibotDriver::cmd_vel_callback(const geometry_msgs::Twist &twist_aux)
{
    SetVelocity(twist_aux.linear.x, twist_aux.linear.y, twist_aux.angular.z);
}

bool RikibotDriver::ReadFormSerial()
{
    if(Robot_Serial.available()){
        rxdata = Robot_Serial.read(Robot_Serial.available());
        if (rxdata.size()==sizeof(RikibotFeedback)){
            memcpy(&Feedback, rxdata.c_str(), sizeof(RikibotFeedback));
            uint16_t checksum = (uint16_t)(Feedback.start^Feedback.rpmR^Feedback.rpmL^Feedback.batVoltage^Feedback.boardTemp^Feedback.curL_DC^Feedback.curR_DC);
            if((Feedback.start == START_FRAME) && (checksum == Feedback.checksum))
                    return true;
            else
                    return false;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

/*底盘速度发送函数*/
void RikibotDriver::SetVelocity(double x, double y, double angular_z)
{
    calculateRPM(x, y, angular_z);
    int16_t mSpeedL = map(req_rpm.motor1, -max_rpm_, max_rpm_, -FOC_MAX_PWM,FOC_MAX_PWM);
    int16_t mSpeedR = map(req_rpm.motor2, -max_rpm_, max_rpm_, -FOC_MAX_PWM,FOC_MAX_PWM);
    Command.start = (uint16_t)START_FRAME;
    Command.mSpeedR = (uint16_t)mSpeedR;
    Command.mSpeedL = (uint16_t)mSpeedL;
    //printf("left rpm: mSpeedL: %d, rrpm: %d\r\n", mSpeedL, mSpeedR);
    Command.checksum = (uint16_t)(Command.start^Command.mSpeedR^Command.mSpeedL);
    Robot_Serial.write((uint8_t *)&Command, sizeof(Command));

}


void RikibotDriver::PublisherMotorVel()
{
    getVelocities(Feedback.rpmL, Feedback.rpmR);	
    raw_vel_msg.linear_x =  vel.linear_x;
    raw_vel_msg.linear_y  = 0;
    raw_vel_msg.angular_z =  vel.angular_z;
    raw_vel_pub_.publish(raw_vel_msg);
}

void RikibotDriver::publisherBattery()
{
    if((ros::Time::now() - now_).toSec() > 1/BATTERY_RATE){
        raw_battery_msg.battery = (float)Feedback.batVoltage/100;
        if (raw_battery_msg.battery != 0){
            battery_pub_.publish(raw_battery_msg);
            now_ = ros::Time::now();
        }
    }
}

void RikibotDriver::calculateRPM(float linear_x, float linear_y, float angular_z)
{
    float linear_vel_x_mins;
    float linear_vel_y_mins;
    float angular_vel_z_mins;
    float tangential_vel;
    float x_rpm;
    float y_rpm;
    float tan_rpm;

    //convert m/s to m/min
    linear_vel_x_mins = linear_x * 60;  
    linear_vel_y_mins = linear_y * 60;

    //convert rad/s to rad/min
    angular_vel_z_mins = angular_z * 60;

    tangential_vel = angular_vel_z_mins * ((wheels_x_distance_ / 2) + (wheels_y_distance_ / 2));

    x_rpm = linear_vel_x_mins / wheel_circumference_;
    y_rpm = linear_vel_y_mins / wheel_circumference_;
    tan_rpm = tangential_vel / wheel_circumference_;

    req_rpm.motor1 = int16_t(x_rpm - y_rpm - tan_rpm);
    req_rpm.motor1 = constrain(req_rpm.motor1, -max_rpm_, max_rpm_);
    //front-right motor
    req_rpm.motor2 = -int16_t(x_rpm + y_rpm + tan_rpm);
    req_rpm.motor2 = constrain(req_rpm.motor2, -max_rpm_, max_rpm_);

}

void RikibotDriver::getVelocities(int rpm1, int rpm2 )
{
    float average_rps_x;
    float average_rps_y;
    float average_rps_a;
    
    //convert average revolutions per minute to revolutions per second
    average_rps_x = ((float)(rpm1 - rpm2) / 2) / 60;  // RPM
    vel.linear_x = average_rps_x * wheel_circumference_; // m/s

    //convert average revolutions per minute in y axis to revolutions per second
    average_rps_y = ((float)(-rpm1 + rpm2) / 2) / 60; // RPM
 
    vel.linear_y = 0;
    
    //convert average revolutions per minute to revolutions per second
    average_rps_a = ((float)(rpm1 + rpm2) / 2) / 60;
    vel.angular_z =  (average_rps_a * wheel_circumference_) / ((wheels_x_distance_ / 2) + (wheels_y_distance_ / 2)); //  rad/s

}


int16_t RikibotDriver::map(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

int main(int argc,char** argv)
{
    ros::init(argc,argv,"Rikibot_driver_node");
    RikibotDriver driver;
    driver.loop();
    return 0;
}
