# 安装依赖环境
> sudo apt-get install ros-melodic-serial

# 如何构建
1、建立ros工作空间
> mkdir ~/foc_ws/src -p

2、拷贝ros1目录下的源码包到工作空间
> copy  rikibot_foc_driver riki_msgs  ~/foc_ws/src -rf

3、编译相关的工作环境

> cd ~/foc_ws
>
> catkin_make  -j1


# 如何使用 
1、首先接好开关按钮，或者信号触发上电驱动板，并接好电机线与控制串口

2、使用前修改rikibot_foc_driver/config/rikibot_params.yaml文件的串口设备好，系统默认是jetson nano的串口名称“/dev/ttyTHS1”

3、初始化工作空间，并启动

> source ~/foc_ws/devel/setup.bash

> roslaunch rikibot_foc_driver rikibot_driver.launch

4、键盘控制电机

> rosrun teleop_twis t_keyboard teleop_twist_keyboard.py

5、查看相关数据,里面反馈了battery(控制板电量)与raw_vel(实时速度)话题

> rostopic list 



