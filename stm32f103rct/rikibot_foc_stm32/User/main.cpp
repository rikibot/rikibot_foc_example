#include "main.h"
#include <timer.h>

int main(void) 
{
	SystemInit();                   //系统初始化
	initialise();                   //SysTick 配置
	motor_en_init();
	MotorFrontSerial.begin(BAUD);
	DebugSerial.begin(BAUD);
	motor_onoff();
	delay(1000);

  while(1){
			RikibotFrontSendData(-200, 200);
			RikibotFrontReceive();	//接收轮毂电机驱动板数据
			delay(100);
   }

}

/* STM32F103RC 72MHZ  256K-FLASH  48K-RAM
    Code:    存在ROM(FLASH)里面的指令，这是在程序运行过程中不变的量，是指令
    RO_data：只读数据(Read only data)，是指令的操作数
    RW_data: 已经初始化的可读写变量的大小，这个存在两个地方，初始化量存在ROM(FLASH)，
         由于还要对其进行"写"操作，所以RAM中也要占有相应空间
    ZI_data: 程序中未初始化的变量大小(Zero Initialize)，存在RAM中

    ROM(FLASH) SIZE:  Code + RO-data + RW-data 
    FLASH SIZE =      28036 + 1620 + 456 = 30.112KB
    FLASH起始地址：0x08000000，所以在设置FLASH进行保存的时候，要大于本代码所占用的扇区地址
    代码扇区大小计算：0x08000000~0x08003fff  ：16*16*16*4=16KB
    所以在设置FLASH地址的时候要在代码flash之外  */


