#include <util.h>

void motor_en_init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RIKI_MOTOR_EN1_GPIO_CLK, ENABLE);  //使能LED 控制管脚的GPIO口时钟
	GPIO_InitStructure.GPIO_Pin     = RIKI_MOTOR_EN1_PIN;     //初始化LED 控制管脚 RIKI_LED_PIN-->PC3
	GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStructure.GPIO_Speed   = GPIO_Speed_2MHz; //IO口速度为50MHz
	GPIO_Init(RIKI_MOTOR_EN1_GPIO_PORT, &GPIO_InitStructure); //根据设定参数初始化RIKI_LED_GPIO_PORT-->PC3
	
	RCC_APB2PeriphClockCmd(RIKI_MOTOR_EN2_GPIO_CLK, ENABLE);  //使能LED 控制管脚的GPIO口时钟
	GPIO_InitStructure.GPIO_Pin     = RIKI_MOTOR_EN2_PIN;     //初始化LED 控制管脚 RIKI_LED_PIN-->PC3
	GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStructure.GPIO_Speed   = GPIO_Speed_50MHz; //IO口速度为50MHz
	GPIO_Init(RIKI_MOTOR_EN2_GPIO_PORT, &GPIO_InitStructure); //根据设定参数初始化RIKI_LED_GPIO_PORT-->PC3
	
	//GPIO_SetBits(RIKI_MOTOR_EN2_GPIO_PORT, RIKI_MOTOR_EN2_PIN); 
	//GPIO_SetBits(RIKI_MOTOR_EN1_GPIO_PORT, RIKI_MOTOR_EN1_PIN); 
}


void motor_onoff()
{
	GPIO_SetBits(RIKI_MOTOR_EN2_GPIO_PORT, RIKI_MOTOR_EN2_PIN); 
	GPIO_SetBits(RIKI_MOTOR_EN1_GPIO_PORT, RIKI_MOTOR_EN1_PIN); 	
	delay(1000);
	GPIO_ResetBits(RIKI_MOTOR_EN2_GPIO_PORT, RIKI_MOTOR_EN2_PIN);
	GPIO_ResetBits(RIKI_MOTOR_EN1_GPIO_PORT, RIKI_MOTOR_EN1_PIN);
}

int map_(float x, float in_min, float in_max, long out_min, long out_max)
{
    return ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

long map(long x, long in_min, long in_max, long out_min, long out_max)
{

 return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

}

long map_abs(long x, long in_min, long in_max, long out_min, long out_max, long value)
{
	long map_value;
  map_value = (abs(x) - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	if(value >= 0)
		return map_value;
	else
		return -map_value;
}

