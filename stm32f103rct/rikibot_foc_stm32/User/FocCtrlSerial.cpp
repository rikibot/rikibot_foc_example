#include<FocCtrlSerial.h>

uint8_t fidx = 0;  
byte *fp, *fsdata;    
byte fincomingByte;
byte fincomingBytePrev;
uint16_t fbufStartFrame, rbufStartFrame;

RikibotCommand FrontCommand;
RikibotFeedback FrontFeedback;

HardwareSerial MotorFrontSerial(SERIAL3);

HardwareSerial DebugSerial(SERIAL4);

int16_t FrpmR=0, FrpmL=0,  RrpmR=0, RrpmL=0;

void RikibotFrontReceive()
{// Check for new data availability in the Serial buffer
    if (MotorFrontSerial.available()) {
        fincomingByte 	  = MotorFrontSerial.read();                                   // Read the incoming byte
			  //DebugSerial.write(incomingByte);
        fbufStartFrame	= ((uint16_t)(fincomingByte) << 8) | fincomingBytePrev;       // Construct the start frame
    }else {
        return;
    }

    // Copy received data
    if (fbufStartFrame == START_FRAME) {	    // Initialize if new data is detected
        fp       = (byte *)&FrontFeedback;
        *fp++    = fincomingBytePrev;
        *fp++    = fincomingByte;
        fidx     = 2;	
    } else if (fidx >= 2 && fidx < sizeof(RikibotFeedback)) {  // Save the new received data
        *fp++    = fincomingByte; 
        fidx++;
    }	  
    // Check if we reached the end of the package
    if (fidx == sizeof(RikibotFeedback)) {
        uint16_t checksum;
        checksum = (uint16_t)(FrontFeedback.start ^ FrontFeedback.rpmR ^ FrontFeedback.rpmL
															^ FrontFeedback.batVoltage ^ FrontFeedback.boardTemp 
															^ FrontFeedback.curL_DC ^ FrontFeedback.curR_DC);
        // Check validity of the new data
        if (FrontFeedback.start == START_FRAME && checksum == FrontFeedback.checksum) {
            //Print data to built-in Serial
            DebugSerial.print(" 右轮转速: %d\r\n", FrontFeedback.rpmR);
            DebugSerial.print(" 左轮转速: %d\r\n", FrontFeedback.rpmL);
            DebugSerial.print(" 电压: %.2f\r\n",		(float)FrontFeedback.batVoltage/100);
            DebugSerial.print(" 主板温度: %.1f\r\n",(float)FrontFeedback.boardTemp/10);
            DebugSerial.print(" 校验值1: %d\r\n",		FrontFeedback.checksum);			
        }
//				else {
//          //DebugSerial.print("Non-valid data skipped\r\n");
//        }
        fidx = 0;    // Reset the index (it prevents to enter in this if condition in the next cycle)
    }

    // Update previous states
    fincomingBytePrev = fincomingByte;

}


void RikibotFrontSendData(int16_t uRight, int16_t uLeft)
{
		FrontCommand.start   	= (uint16_t)START_FRAME;
		FrontCommand.mSpeedR   = (int16_t)uRight;
		FrontCommand.mSpeedL   = (int16_t)uLeft;
		FrontCommand.checksum 	= (uint16_t)(FrontCommand.start ^ FrontCommand.mSpeedR ^ FrontCommand.mSpeedL);

    MotorFrontSerial.write((uint8_t *) &FrontCommand, sizeof(FrontCommand)); 
}

