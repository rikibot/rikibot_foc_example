#ifdef __cplusplus
extern "C" {
#endif

#ifndef ROSNODESERIAL_H
#define ROSNODESERIAL_H
#include <config.h>
#include <hardwareserial.h>
#include <riki_msgs/Imu.h>
#include <riki_msgs/Velocities.h> 
#include <riki_msgs/Battery.h>
#include <riki_msgs/Sonar.h>

#if SERIAL_MODE   
extern HardwareSerial RosSerial;
#endif
//Frame 0xABCD
typedef struct{
   uint16_t start;
   uint16_t checksum;
   float  vel_x;
   float  vel_y;
   float  angular_z;
} RosNodeFeedback;

#pragma pack(4)
typedef struct {
	float x;
	float y;
	float z;
}SensorData;

#pragma pack(4)
typedef struct{
    uint16_t   start;
    uint16_t   checksum;
		float      sonar;
    float      volt; 
	  SensorData raw_vel;
    SensorData mpu_accel;
    SensorData mpu_gyro;
}RosNodeCommand;


void RikiRosReceiveData();
void RikiRosSendData();
uint16_t check_sum(byte *data);
#endif

#ifdef __cplusplus
}
#endif