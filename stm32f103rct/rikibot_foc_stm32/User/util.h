#ifdef __cplusplus
extern "C" {
#endif

#ifndef UTIL_H
#define UTIL_H
#include <config.h>

void motor_en_init();
void motor_onoff();
int map_(float x, float in_min, float in_max, long out_min, long out_max);
long map(long x, long in_min, long in_max, long out_min, long out_max);
long map_abs(long x, long in_min, long in_max, long out_min, long out_max,  long value);
float mapFloat(float x, float in_min, float in_max, float out_min, float out_max);


#endif
	
#ifdef __cplusplus
}
#endif
