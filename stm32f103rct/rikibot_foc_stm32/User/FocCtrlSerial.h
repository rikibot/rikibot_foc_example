#ifdef __cplusplus
extern "C" {
#endif

#ifndef FOCCTRLSERIAL_H
#define FOCCTRLSERIAL_H
#include <config.h>
#include <hardwareserial.h>


extern RikibotCommand FrontCommand;
extern RikibotFeedback FrontFeedback;

extern HardwareSerial MotorFrontSerial;

//#if SERIAL_DEBUG                    //��ͨ�����������
extern HardwareSerial DebugSerial;
extern int16_t FrpmR, FrpmL;
//#endif
void RikibotFrontReceive();
void RikibotFrontSendData(int16_t uRight, int16_t uLeft);


#endif
	
#ifdef __cplusplus
}
#endif