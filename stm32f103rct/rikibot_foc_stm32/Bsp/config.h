#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>               
#include "stm32f10x.h"          
#include "ring_buffer.h"        
#include "millisecondtimer.h"  


typedef uint8_t byte;

#define PS2_DEBUG_    0
#define PS2_X_VALUE_  128
#define PS2_Y_VALUE_  127

#define RC_MIN_VALUE   1000
#define RC_MAX_VALUE   2000
#define MAX_SPEED_X		 2.0
#define MAX_SPEED_Z		 3.0


#define PI      			3.1415926


//串口通信模式选择  默认是ROSSERIAL模式，如果需要进行相关调试，可以开启普通单片机串口输出模式  此时可用Windowns串口助手接收相关输出信息
#define SERIAL_MODE             1     //普通单片机串口输出模式      
#define ROSSERIAL               0      //ROS rosserial通信模式 


#define DEBUG   								1


#define RIKIBOT_2WD   							 //2轮驱动定义
//#define RIKIBOT_4WD

typedef struct{                //接收数据结构体
    uint16_t  start;          //0xABCD
    int16_t   rpmR;    				//右轮转速
    int16_t   rpmL;    				//左轮转速
    int16_t   batVoltage;     //主板电压
    int16_t   boardTemp;      //主板温度
    int16_t 	curL_DC;        //左电机电流
    int16_t 	curR_DC;        //右电机电流
    uint16_t  checksum;       //校验和
} RikibotFeedback;

typedef struct{           //发送数据结构体
   uint16_t start;        //0xABCD
   int16_t  mSpeedR;      //发送右侧电机目标转速
   int16_t  mSpeedL;      //发送左侧电机目标转速
   uint16_t checksum;     //校验和
} RikibotCommand;
 

#define START_FRAME         0xABCD     	// [-] Start frme definition for reliable serial communication 帧头

#define IMU_PUBLISH_RATE        40  //hz
#define PS2JOY_RATE          		20  //hz
#define BAT_PUBLISH_RATE        1 //hz
#define COMMAND_RATE            20  //hz
#define DEBUG_RATE              1   //hz
#define SERAIL_COMMUNICATION_RATE 50

/** motor param **/
#define MAX_RPM         366     //motor's maximum RPM    0.78m/s
#define FOC_MAX_PWM			1000


#ifdef RIKIBOT_2WD
#define LR_WHEELS_DISTANCE 0.33
#define FR_WHEELS_DISTANCE 0.0
#define WHEEL_DIAMETER  	 0.165   //wheel's diameter in meters   0.545m / 3.14    0.173479
#endif

#ifdef RIKIBOT_4WD
#define LR_WHEELS_DISTANCE 0.48
#define FR_WHEELS_DISTANCE 0.28
#define WHEEL_DIAMETER  	 0.2   //wheel's diameter in meters   0.545m / 3.14    0.173479
#endif



#define 	USE_SERIAL1
#define 	USE_SERIAL2
#define 	USE_SERIAL3
#define 	USE_SERIAL4
#define   USE_I2C
#define   USE_SERVO1
#define   USE_SERVO2
#define   USE_SONAR
#define  	USE_RC


/** --------Serial Config-------- **/
typedef enum {
	SERIAL1 = 0,
	SERIAL2 = 1,
	SERIAL3 = 2,
	SERIAL4 = 3,
	SERIAL_END = 4
}Serial_TypeDef; 

#define SERIALn												4       

#define RIKI_SERIAL1									USART1  
#define RIKI_SERIAL1_IRQ							USART1_IRQn
#define RIKI_SERIAL1_CLK             	RCC_APB2Periph_USART1
#define RIKI_SERIAL1_GPIO_CLK         RCC_APB2Periph_GPIOA
#define RIKI_SERIAL1_GPIO_PORT        GPIOA
#define RIKI_SERIAL1_TX_PIN           GPIO_Pin_9
#define RIKI_SERIAL1_RX_PIN           GPIO_Pin_10
#define RIKI_SERIAL1_NVIC							0

#define RIKI_SERIAL2									USART2  
#define RIKI_SERIAL2_IRQ							USART2_IRQn
#define RIKI_SERIAL2_CLK             	RCC_APB1Periph_USART2
#define RIKI_SERIAL2_GPIO_CLK         RCC_APB2Periph_GPIOA
#define RIKI_SERIAL2_GPIO_PORT        GPIOA
#define RIKI_SERIAL2_TX_PIN           GPIO_Pin_2
#define RIKI_SERIAL2_RX_PIN           GPIO_Pin_3
#define RIKI_SERIAL2_NVIC							1

#define RIKI_SERIAL3                  USART3
#define RIKI_SERIAL3_IRQ              USART3_IRQn
#define RIKI_SERIAL3_CLK             	RCC_APB1Periph_USART3
#define RIKI_SERIAL3_GPIO_CLK        	RCC_APB2Periph_GPIOB
#define RIKI_SERIAL3_GPIO_PORT      	GPIOB
#define RIKI_SERIAL3_TX_PIN           GPIO_Pin_10
#define RIKI_SERIAL3_RX_PIN           GPIO_Pin_11
#define RIKI_SERIAL3_NVIC							2

#define RIKI_SERIAL4                  UART4
#define RIKI_SERIAL4_IRQ              UART4_IRQn
#define RIKI_SERIAL4_CLK             	RCC_APB1Periph_UART4
#define RIKI_SERIAL4_GPIO_CLK        	RCC_APB2Periph_GPIOC
#define RIKI_SERIAL4_GPIO_PORT      	GPIOC
#define RIKI_SERIAL4_TX_PIN           GPIO_Pin_10
#define RIKI_SERIAL4_RX_PIN           GPIO_Pin_11
#define RIKI_SERIAL4_NVIC							3

#define RXBUF_SIZE        						512
#define BAUD													115200


//PS2
//接收器管脚    板子               接板子丝印   
//CLK           PA4                 CLK
//CS            PA5                 CS
//VDD                               VCC（不能5V供电！！）
//GND                               GND
//DO            PC4                 D0  
//DI            PC5                 DI


#define 	AXES_LEN    4
#define 	JOYBTN_LEN  16
#define 	PS2_UPPER   150
#define 	PS2_LOWER   100

#define   PS2_CLK_PIN   GPIO_Pin_4
#define   PS2_CS_PIN    GPIO_Pin_5
#define   PS2_DI_PIN    GPIO_Pin_7
#define   PS2_DO_PIN    GPIO_Pin_6

#define 	PS2_CLK_GPIO_PORT   GPIOA  
#define 	PS2_CS_GPIO_PORT    GPIOA
#define 	PS2_DI_GPIO_PORT    GPIOA
#define 	PS2_DO_GPIO_PORT    GPIOA

#define 	PS2_CLK_GPIO_CLK    RCC_APB2Periph_GPIOA
#define 	PS2_CS_GPIO_CLK     RCC_APB2Periph_GPIOA
#define 	PS2_DI_GPIO_CLK     RCC_APB2Periph_GPIOA
#define 	PS2_DO_GPIO_CLK     RCC_APB2Periph_GPIOA


/* IMU TYPE CHOOSE */
#define USE_MPU6050_IMU

/** I2C Config**/
/* MPU6050 */
#define RIKI_SDA_PIN              GPIO_Pin_6
#define RIKI_SCL_PIN              GPIO_Pin_5
#define RIKI_I2C_GPIO_PORT        GPIOB
#define RIKI_I2C_GPIO_CLK         RCC_APB2Periph_GPIOB
#define SDA_IN()  {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=8<<(1*24);}   //PB9输入模式
#define SDA_OUT() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=3<<(1*24);}   //PB9输出模式


/* DHT22 Config*/
#define RIKI_DHT22_IO_IN()      {GPIOA->CRH&=0X0FFFFFFF;GPIOA->CRH|=8<<28;} 
#define RIKI_DHT22_IO_OUT()     {GPIOA->CRH&=0X0FFFFFFF;GPIOA->CRH|=3<<28;}
#define	RIKI_DHT22_DQ_OUT       PAout(15)     
#define	RIKI_DHT22_DQ_IN        PAin(15)       
#define RIKI_DHT22_PIN          GPIO_Pin_15
#define RIKI_DHT22_GPIO_PORT    GPIOA
#define RIKI_DHT22_GPIO_CLK     RCC_APB2Periph_GPIOA

/** Servo Config **/
typedef enum {
	SERVO1 = 0,
	SERVO2 = 1,
	SERVO_END = 2
}Servo_TypeDef; 

#define SERVOn 					2
#define CENTER_POS			90
#define MAX_ANGLE				180

#define RIKI_SERVO1_PIN					GPIO_Pin_8              //PB8-TIM4_CH3
#define RIKI_SERVO1_GPIO_PORT		GPIOB
#define RIKI_SERVO1_GPIO_CLK		RCC_APB2Periph_GPIOB
#define RIKI_SERVO1_TIM					TIM4
#define RIKI_SERVO1_TIM_CLK			RCC_APB1Periph_TIM4

#define RIKI_SERVO2_PIN					GPIO_Pin_9              //PB9-TIM4_CH4
#define RIKI_SERVO2_GPIO_PORT		GPIOB                   
#define RIKI_SERVO2_GPIO_CLK		RCC_APB2Periph_GPIOB    
#define RIKI_SERVO2_TIM					TIM4
#define RIKI_SERVO2_TIM_CLK			RCC_APB1Periph_TIM4

/** LED config **/
#define RIKI_LED_PIN            GPIO_Pin_4
#define RIKI_LED_GPIO_PORT			GPIOC
#define RIKI_LED_GPIO_CLK				RCC_APB2Periph_GPIOC

/** BUZ config **/
#define RIKI_BUZ_PIN            GPIO_Pin_0
#define RIKI_BUZ_GPIO_PORT			GPIOB
#define RIKI_BUZ_GPIO_CLK				RCC_APB2Periph_GPIOB

/** Motor enable config **/
#define RIKI_MOTOR_EN1_PIN            GPIO_Pin_13
#define RIKI_MOTOR_EN1_GPIO_PORT			GPIOC
#define RIKI_MOTOR_EN1_GPIO_CLK				RCC_APB2Periph_GPIOC

#define RIKI_MOTOR_EN2_PIN            GPIO_Pin_5
#define RIKI_MOTOR_EN2_GPIO_PORT			GPIOC
#define RIKI_MOTOR_EN2_GPIO_CLK				RCC_APB2Periph_GPIOC

/** volt adc config **/
#define ADC1_DR_ADDRESS             ((u32)0x4001244C)
#define RIKI_BATTERY_PIN            GPIO_Pin_0
#define RIKI_BATTERY_GPIO_PORT      GPIOC
#define RIKI_BATTERY_GPIO_CLK       RCC_APB2Periph_GPIOC
#define RIKI_BATTERY_ADC_CLK        RCC_APB2Periph_ADC1
#define RIKI_BATTERY_DMA_CLK        RCC_AHBPeriph_DMA1

/** Sonar config **/
typedef enum {
	SONAR1 = 0,
//	SONAR2 = 1,
//	SONAR3 = 2,
//	SONAR4 = 3,
	SONAR_END = 1
}Sonar_TypeDef; 

#define SONARn   1

#define RIKI_ECHO1_PIN               GPIO_Pin_8  //TIM1_CH1
#define RIKI_TRIG1_PIN               GPIO_Pin_15
#define RIKI_ECHO1_GPIO_PORT         GPIOA
#define RIKI_ECHO1_GPIO_CLK          RCC_APB2Periph_GPIOA
#define RIKI_TRIG1_GPIO_PORT         GPIOB
#define RIKI_TRIG1_GPIO_CLK          RCC_APB2Periph_GPIOB

//#define RIKI_ECHO2_PIN               GPIO_Pin_7  //TIM1_CH1
//#define RIKI_TRIG2_PIN               GPIO_Pin_15
//#define RIKI_ECHO2_GPIO_PORT         GPIOC
//#define RIKI_ECHO2_GPIO_CLK          RCC_APB2Periph_GPIOC
//#define RIKI_TRIG2_GPIO_PORT         GPIOB
//#define RIKI_TRIG2_GPIO_CLK          RCC_APB2Periph_GPIOB

//#define RIKI_ECHO3_PIN               GPIO_Pin_8  //TIM1_CH1
//#define RIKI_TRIG3_PIN               GPIO_Pin_15
//#define RIKI_ECHO3_GPIO_PORT         GPIOC
//#define RIKI_ECHO3_GPIO_CLK          RCC_APB2Periph_GPIOC
//#define RIKI_TRIG3_GPIO_PORT         GPIOA
//#define RIKI_TRIG3_GPIO_CLK          RCC_APB2Periph_GPIOA

//#define RIKI_ECHO4_PIN               GPIO_Pin_9  //TIM1_CH1
//#define RIKI_TRIG4_PIN               GPIO_Pin_8
//#define RIKI_ECHO4_GPIO_PORT         GPIOC
//#define RIKI_ECHO4_GPIO_CLK          RCC_APB2Periph_GPIOC
//#define RIKI_TRIG4_GPIO_PORT         GPIOA
//#define RIKI_TRIG4_GPIO_CLK          RCC_APB2Periph_GPIOA


#define RIKI_SONAR_TIM               TIM1
#define RIKI_SONAR_TIM_CLK           RCC_APB2Periph_TIM1
#define RIKI_SONAR_TIM_IRQ           TIM1_CC_IRQn


#define RIKI_RCCH1_PIN               GPIO_Pin_9  //TIM8_CH4
#define RIKI_RCCH2_PIN               GPIO_Pin_8  //TIM8_CH3
#define RIKI_RCCH3_PIN               GPIO_Pin_7  //TIM8_CH2
#define RIKI_RCCH4_PIN               GPIO_Pin_6  //TIM8_CH1

#define RIKI_RCCH_GPIO_PORT         	GPIOC
#define RIKI_RCCH_GPIO_CLK          	RCC_APB2Periph_GPIOC

//#define RIKI_RCCH5_PIN               GPIO_Pin_1  //TIM3_CH2
//#define RIKI_RCCH5_GPIO_PORT         GPIOB
//#define RIKI_RCCH5_GPIO_CLK          RCC_APB2Periph_GPIOB

//#define RIKI_RCCH6_PIN               GPIO_Pin_0  //TIM3_CH1
//#define RIKI_RCCH6_GPIO_PORT         GPIOB
//#define RIKI_RCCH6_GPIO_CLK          RCC_APB2Periph_GPIOB



#define RIKI_RC_TIM               TIM8
#define RIKI_RC_TIM_CLK           RCC_APB2Periph_TIM8
#define RIKI_RC_TIM_IRQ           TIM8_CC_IRQn

//#define RIKI_RC2_TIM               TIM3
//#define RIKI_RC2_TIM_CLK           RCC_APB2Periph_TIM3
//#define RIKI_RC2_TIM_IRQ           TIM3_IRQn


/** GPIO Bit Config **/
#define BITBAND(addr, bitnum)   ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)  MEM_ADDR(BITBAND(addr, bitnum)) 
 
#define GPIOA_ODR_Addr    (GPIOA_BASE+12)  
#define GPIOB_ODR_Addr    (GPIOB_BASE+12)  
#define GPIOC_ODR_Addr    (GPIOC_BASE+12)  
#define GPIOD_ODR_Addr    (GPIOD_BASE+12)   
#define GPIOE_ODR_Addr    (GPIOE_BASE+12)  
#define GPIOF_ODR_Addr    (GPIOF_BASE+12)     
#define GPIOG_ODR_Addr    (GPIOG_BASE+12)  

#define GPIOA_IDR_Addr    (GPIOA_BASE+8)  
#define GPIOB_IDR_Addr    (GPIOB_BASE+8)  
#define GPIOC_IDR_Addr    (GPIOC_BASE+8)   
#define GPIOD_IDR_Addr    (GPIOD_BASE+8)  
#define GPIOE_IDR_Addr    (GPIOE_BASE+8)   
#define GPIOF_IDR_Addr    (GPIOF_BASE+8)  
#define GPIOG_IDR_Addr    (GPIOG_BASE+8)   

#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)   
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n) 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)   
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n) 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)   
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)   

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)   

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)   

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n) 

#endif // _CONFIG_H_

