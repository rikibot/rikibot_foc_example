#ifndef _ROS_SERVICE_FaceVoiceSet_h
#define _ROS_SERVICE_FaceVoiceSet_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

static const char FACEVOICESET[] = "riki_msgs/FaceVoiceSet";

  class FaceVoiceSetRequest : public ros::Msg
  {
    public:
      typedef int32_t _voice_id_type;
      _voice_id_type voice_id;

    FaceVoiceSetRequest():
      voice_id(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_voice_id;
      u_voice_id.real = this->voice_id;
      *(outbuffer + offset + 0) = (u_voice_id.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_voice_id.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_voice_id.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_voice_id.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->voice_id);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_voice_id;
      u_voice_id.base = 0;
      u_voice_id.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_voice_id.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_voice_id.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_voice_id.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->voice_id = u_voice_id.real;
      offset += sizeof(this->voice_id);
     return offset;
    }

    const char * getType(){ return FACEVOICESET; };
    const char * getMD5(){ return "98325de2d7d0e1dc65e6817d659bf621"; };

  };

  class FaceVoiceSetResponse : public ros::Msg
  {
    public:
      typedef bool _face_id_type;
      _face_id_type face_id;
      typedef const char* _name_type;
      _name_type name;

    FaceVoiceSetResponse():
      face_id(0),
      name("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_face_id;
      u_face_id.real = this->face_id;
      *(outbuffer + offset + 0) = (u_face_id.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->face_id);
      uint32_t length_name = strlen(this->name);
      varToArr(outbuffer + offset, length_name);
      offset += 4;
      memcpy(outbuffer + offset, this->name, length_name);
      offset += length_name;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_face_id;
      u_face_id.base = 0;
      u_face_id.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->face_id = u_face_id.real;
      offset += sizeof(this->face_id);
      uint32_t length_name;
      arrToVar(length_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_name-1]=0;
      this->name = (char *)(inbuffer + offset-1);
      offset += length_name;
     return offset;
    }

    const char * getType(){ return FACEVOICESET; };
    const char * getMD5(){ return "0287046282234b95fe7e07ac1a8feb81"; };

  };

  class FaceVoiceSet {
    public:
    typedef FaceVoiceSetRequest Request;
    typedef FaceVoiceSetResponse Response;
  };

}
#endif
