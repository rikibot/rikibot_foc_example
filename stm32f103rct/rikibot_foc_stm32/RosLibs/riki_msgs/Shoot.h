#ifndef _ROS_SERVICE_Shoot_h
#define _ROS_SERVICE_Shoot_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

static const char SHOOT[] = "riki_msgs/Shoot";

  class ShootRequest : public ros::Msg
  {
    public:
      typedef int16_t _time_type;
      _time_type time;

    ShootRequest():
      time(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.real = this->time;
      *(outbuffer + offset + 0) = (u_time.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_time.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->time);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.base = 0;
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->time = u_time.real;
      offset += sizeof(this->time);
     return offset;
    }

    const char * getType(){ return SHOOT; };
    const char * getMD5(){ return "b8e8bf26b20382c114e84bf5da39d6e4"; };

  };

  class ShootResponse : public ros::Msg
  {
    public:

    ShootResponse()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return SHOOT; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class Shoot {
    public:
    typedef ShootRequest Request;
    typedef ShootResponse Response;
  };

}
#endif
