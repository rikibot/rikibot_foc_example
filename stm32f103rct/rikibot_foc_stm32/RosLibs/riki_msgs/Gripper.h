#ifndef _ROS_SERVICE_Gripper_h
#define _ROS_SERVICE_Gripper_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

static const char GRIPPER[] = "riki_msgs/Gripper";

  class GripperRequest : public ros::Msg
  {
    public:
      typedef int16_t _step_type;
      _step_type step;
      typedef int16_t _time_type;
      _time_type time;

    GripperRequest():
      step(0),
      time(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_step;
      u_step.real = this->step;
      *(outbuffer + offset + 0) = (u_step.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_step.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->step);
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.real = this->time;
      *(outbuffer + offset + 0) = (u_time.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_time.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->time);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_step;
      u_step.base = 0;
      u_step.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_step.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->step = u_step.real;
      offset += sizeof(this->step);
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.base = 0;
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->time = u_time.real;
      offset += sizeof(this->time);
     return offset;
    }

    const char * getType(){ return GRIPPER; };
    const char * getMD5(){ return "e27b20d61c16bcd76ef982854742171b"; };

  };

  class GripperResponse : public ros::Msg
  {
    public:

    GripperResponse()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return GRIPPER; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class Gripper {
    public:
    typedef GripperRequest Request;
    typedef GripperResponse Response;
  };

}
#endif
