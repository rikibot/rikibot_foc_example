#ifndef _ROS_riki_msgs_Bldc_data_h
#define _ROS_riki_msgs_Bldc_data_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

  class Bldc_data : public ros::Msg
  {
    public:
      typedef float _Right_Motor_rpm_type;
      _Right_Motor_rpm_type Right_Motor_rpm;
      typedef float _Left_Motor_rpm_type;
      _Left_Motor_rpm_type Left_Motor_rpm;
      typedef float _Right_Motor_Current_type;
      _Right_Motor_Current_type Right_Motor_Current;
      typedef float _Left_Motor_Current_type;
      _Left_Motor_Current_type Left_Motor_Current;
      typedef float _battery_voltage_type;
      _battery_voltage_type battery_voltage;
      typedef float _board_temperature_type;
      _board_temperature_type board_temperature;

    Bldc_data():
      Right_Motor_rpm(0),
      Left_Motor_rpm(0),
      Right_Motor_Current(0),
      Left_Motor_Current(0),
      battery_voltage(0),
      board_temperature(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_Right_Motor_rpm;
      u_Right_Motor_rpm.real = this->Right_Motor_rpm;
      *(outbuffer + offset + 0) = (u_Right_Motor_rpm.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_Right_Motor_rpm.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_Right_Motor_rpm.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_Right_Motor_rpm.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->Right_Motor_rpm);
      union {
        float real;
        uint32_t base;
      } u_Left_Motor_rpm;
      u_Left_Motor_rpm.real = this->Left_Motor_rpm;
      *(outbuffer + offset + 0) = (u_Left_Motor_rpm.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_Left_Motor_rpm.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_Left_Motor_rpm.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_Left_Motor_rpm.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->Left_Motor_rpm);
      union {
        float real;
        uint32_t base;
      } u_Right_Motor_Current;
      u_Right_Motor_Current.real = this->Right_Motor_Current;
      *(outbuffer + offset + 0) = (u_Right_Motor_Current.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_Right_Motor_Current.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_Right_Motor_Current.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_Right_Motor_Current.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->Right_Motor_Current);
      union {
        float real;
        uint32_t base;
      } u_Left_Motor_Current;
      u_Left_Motor_Current.real = this->Left_Motor_Current;
      *(outbuffer + offset + 0) = (u_Left_Motor_Current.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_Left_Motor_Current.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_Left_Motor_Current.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_Left_Motor_Current.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->Left_Motor_Current);
      union {
        float real;
        uint32_t base;
      } u_battery_voltage;
      u_battery_voltage.real = this->battery_voltage;
      *(outbuffer + offset + 0) = (u_battery_voltage.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_battery_voltage.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_battery_voltage.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_battery_voltage.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->battery_voltage);
      union {
        float real;
        uint32_t base;
      } u_board_temperature;
      u_board_temperature.real = this->board_temperature;
      *(outbuffer + offset + 0) = (u_board_temperature.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_board_temperature.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_board_temperature.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_board_temperature.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->board_temperature);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_Right_Motor_rpm;
      u_Right_Motor_rpm.base = 0;
      u_Right_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_Right_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_Right_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_Right_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->Right_Motor_rpm = u_Right_Motor_rpm.real;
      offset += sizeof(this->Right_Motor_rpm);
      union {
        float real;
        uint32_t base;
      } u_Left_Motor_rpm;
      u_Left_Motor_rpm.base = 0;
      u_Left_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_Left_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_Left_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_Left_Motor_rpm.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->Left_Motor_rpm = u_Left_Motor_rpm.real;
      offset += sizeof(this->Left_Motor_rpm);
      union {
        float real;
        uint32_t base;
      } u_Right_Motor_Current;
      u_Right_Motor_Current.base = 0;
      u_Right_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_Right_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_Right_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_Right_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->Right_Motor_Current = u_Right_Motor_Current.real;
      offset += sizeof(this->Right_Motor_Current);
      union {
        float real;
        uint32_t base;
      } u_Left_Motor_Current;
      u_Left_Motor_Current.base = 0;
      u_Left_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_Left_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_Left_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_Left_Motor_Current.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->Left_Motor_Current = u_Left_Motor_Current.real;
      offset += sizeof(this->Left_Motor_Current);
      union {
        float real;
        uint32_t base;
      } u_battery_voltage;
      u_battery_voltage.base = 0;
      u_battery_voltage.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_battery_voltage.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_battery_voltage.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_battery_voltage.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->battery_voltage = u_battery_voltage.real;
      offset += sizeof(this->battery_voltage);
      union {
        float real;
        uint32_t base;
      } u_board_temperature;
      u_board_temperature.base = 0;
      u_board_temperature.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_board_temperature.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_board_temperature.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_board_temperature.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->board_temperature = u_board_temperature.real;
      offset += sizeof(this->board_temperature);
     return offset;
    }

    const char * getType(){ return "riki_msgs/Bldc_data"; };
    const char * getMD5(){ return "cf61970032de7546621f1e9a6026edd8"; };

  };

}
#endif
