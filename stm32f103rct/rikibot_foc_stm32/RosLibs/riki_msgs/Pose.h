#ifndef _ROS_SERVICE_Pose_h
#define _ROS_SERVICE_Pose_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

static const char POSE[] = "riki_msgs/Pose";

  class PoseRequest : public ros::Msg
  {
    public:
      typedef float _x_type;
      _x_type x;
      typedef float _y_type;
      _y_type y;
      typedef float _z_type;
      _z_type z;
      typedef float _rtoary_type;
      _rtoary_type rtoary;
      typedef int16_t _time_type;
      _time_type time;

    PoseRequest():
      x(0),
      y(0),
      z(0),
      rtoary(0),
      time(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_x;
      u_x.real = this->x;
      *(outbuffer + offset + 0) = (u_x.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_x.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_x.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_x.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->x);
      union {
        float real;
        uint32_t base;
      } u_y;
      u_y.real = this->y;
      *(outbuffer + offset + 0) = (u_y.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_y.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_y.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_y.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->y);
      union {
        float real;
        uint32_t base;
      } u_z;
      u_z.real = this->z;
      *(outbuffer + offset + 0) = (u_z.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_z.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_z.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_z.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->z);
      union {
        float real;
        uint32_t base;
      } u_rtoary;
      u_rtoary.real = this->rtoary;
      *(outbuffer + offset + 0) = (u_rtoary.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_rtoary.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_rtoary.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_rtoary.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->rtoary);
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.real = this->time;
      *(outbuffer + offset + 0) = (u_time.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_time.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->time);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_x;
      u_x.base = 0;
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_x.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->x = u_x.real;
      offset += sizeof(this->x);
      union {
        float real;
        uint32_t base;
      } u_y;
      u_y.base = 0;
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_y.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->y = u_y.real;
      offset += sizeof(this->y);
      union {
        float real;
        uint32_t base;
      } u_z;
      u_z.base = 0;
      u_z.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_z.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_z.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_z.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->z = u_z.real;
      offset += sizeof(this->z);
      union {
        float real;
        uint32_t base;
      } u_rtoary;
      u_rtoary.base = 0;
      u_rtoary.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_rtoary.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_rtoary.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_rtoary.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->rtoary = u_rtoary.real;
      offset += sizeof(this->rtoary);
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.base = 0;
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->time = u_time.real;
      offset += sizeof(this->time);
     return offset;
    }

    const char * getType(){ return POSE; };
    const char * getMD5(){ return "c4258fd17a5d5267d5a22ad00faa1382"; };

  };

  class PoseResponse : public ros::Msg
  {
    public:

    PoseResponse()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return POSE; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class Pose {
    public:
    typedef PoseRequest Request;
    typedef PoseResponse Response;
  };

}
#endif
