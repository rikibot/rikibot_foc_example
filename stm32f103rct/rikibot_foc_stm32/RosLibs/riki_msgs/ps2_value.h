#ifndef _ROS_riki_msgs_ps2_value_h
#define _ROS_riki_msgs_ps2_value_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

  class ps2_value : public ros::Msg
  {
    public:
      uint8_t data[10];

    ps2_value():
      data()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      for( uint32_t i = 0; i < 10; i++){
      *(outbuffer + offset + 0) = (this->data[i] >> (8 * 0)) & 0xFF;
      offset += sizeof(this->data[i]);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      for( uint32_t i = 0; i < 10; i++){
      this->data[i] =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->data[i]);
      }
     return offset;
    }

    const char * getType(){ return "riki_msgs/ps2_value"; };
    const char * getMD5(){ return "0c05a05733f13fb160c661ca1798fdba"; };

  };

}
#endif
