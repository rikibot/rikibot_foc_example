#ifndef _ROS_SERVICE_RobotVoiceCtrl_h
#define _ROS_SERVICE_RobotVoiceCtrl_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

static const char ROBOTVOICECTRL[] = "riki_msgs/RobotVoiceCtrl";

  class RobotVoiceCtrlRequest : public ros::Msg
  {
    public:
      typedef int32_t _ctrl_id_type;
      _ctrl_id_type ctrl_id;

    RobotVoiceCtrlRequest():
      ctrl_id(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_ctrl_id;
      u_ctrl_id.real = this->ctrl_id;
      *(outbuffer + offset + 0) = (u_ctrl_id.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_ctrl_id.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_ctrl_id.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_ctrl_id.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->ctrl_id);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_ctrl_id;
      u_ctrl_id.base = 0;
      u_ctrl_id.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_ctrl_id.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_ctrl_id.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_ctrl_id.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->ctrl_id = u_ctrl_id.real;
      offset += sizeof(this->ctrl_id);
     return offset;
    }

    const char * getType(){ return ROBOTVOICECTRL; };
    const char * getMD5(){ return "11f6f9365c6a32e4d7e2a5c7cc4c8895"; };

  };

  class RobotVoiceCtrlResponse : public ros::Msg
  {
    public:
      typedef bool _face_id_type;
      _face_id_type face_id;

    RobotVoiceCtrlResponse():
      face_id(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_face_id;
      u_face_id.real = this->face_id;
      *(outbuffer + offset + 0) = (u_face_id.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->face_id);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_face_id;
      u_face_id.base = 0;
      u_face_id.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->face_id = u_face_id.real;
      offset += sizeof(this->face_id);
     return offset;
    }

    const char * getType(){ return ROBOTVOICECTRL; };
    const char * getMD5(){ return "337f2aabe2cbd4bf896f44421c3c0179"; };

  };

  class RobotVoiceCtrl {
    public:
    typedef RobotVoiceCtrlRequest Request;
    typedef RobotVoiceCtrlResponse Response;
  };

}
#endif
