#ifndef _ROS_SERVICE_Trajectory_h
#define _ROS_SERVICE_Trajectory_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

static const char TRAJECTORY[] = "riki_msgs/Trajectory";

  class TrajectoryRequest : public ros::Msg
  {
    public:
      typedef int16_t _id2_type;
      _id2_type id2;
      typedef int16_t _id3_type;
      _id3_type id3;
      typedef int16_t _id4_type;
      _id4_type id4;
      typedef int16_t _id5_type;
      _id5_type id5;
      typedef int16_t _id6_type;
      _id6_type id6;
      typedef int16_t _time_type;
      _time_type time;

    TrajectoryRequest():
      id2(0),
      id3(0),
      id4(0),
      id5(0),
      id6(0),
      time(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_id2;
      u_id2.real = this->id2;
      *(outbuffer + offset + 0) = (u_id2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_id2.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->id2);
      union {
        int16_t real;
        uint16_t base;
      } u_id3;
      u_id3.real = this->id3;
      *(outbuffer + offset + 0) = (u_id3.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_id3.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->id3);
      union {
        int16_t real;
        uint16_t base;
      } u_id4;
      u_id4.real = this->id4;
      *(outbuffer + offset + 0) = (u_id4.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_id4.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->id4);
      union {
        int16_t real;
        uint16_t base;
      } u_id5;
      u_id5.real = this->id5;
      *(outbuffer + offset + 0) = (u_id5.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_id5.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->id5);
      union {
        int16_t real;
        uint16_t base;
      } u_id6;
      u_id6.real = this->id6;
      *(outbuffer + offset + 0) = (u_id6.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_id6.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->id6);
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.real = this->time;
      *(outbuffer + offset + 0) = (u_time.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_time.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->time);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int16_t real;
        uint16_t base;
      } u_id2;
      u_id2.base = 0;
      u_id2.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_id2.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->id2 = u_id2.real;
      offset += sizeof(this->id2);
      union {
        int16_t real;
        uint16_t base;
      } u_id3;
      u_id3.base = 0;
      u_id3.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_id3.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->id3 = u_id3.real;
      offset += sizeof(this->id3);
      union {
        int16_t real;
        uint16_t base;
      } u_id4;
      u_id4.base = 0;
      u_id4.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_id4.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->id4 = u_id4.real;
      offset += sizeof(this->id4);
      union {
        int16_t real;
        uint16_t base;
      } u_id5;
      u_id5.base = 0;
      u_id5.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_id5.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->id5 = u_id5.real;
      offset += sizeof(this->id5);
      union {
        int16_t real;
        uint16_t base;
      } u_id6;
      u_id6.base = 0;
      u_id6.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_id6.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->id6 = u_id6.real;
      offset += sizeof(this->id6);
      union {
        int16_t real;
        uint16_t base;
      } u_time;
      u_time.base = 0;
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_time.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->time = u_time.real;
      offset += sizeof(this->time);
     return offset;
    }

    const char * getType(){ return TRAJECTORY; };
    const char * getMD5(){ return "f982c44c4992b0945e9540dc9fc648e5"; };

  };

  class TrajectoryResponse : public ros::Msg
  {
    public:

    TrajectoryResponse()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return TRAJECTORY; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class Trajectory {
    public:
    typedef TrajectoryRequest Request;
    typedef TrajectoryResponse Response;
  };

}
#endif
