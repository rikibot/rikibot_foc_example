#ifndef _ROS_riki_msgs_Led_h
#define _ROS_riki_msgs_Led_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

  class Led : public ros::Msg
  {
    public:
      typedef int32_t _LED1_type;
      _LED1_type LED1;
      typedef int32_t _LED2_type;
      _LED2_type LED2;
      typedef int32_t _LED3_type;
      _LED3_type LED3;

    Led():
      LED1(0),
      LED2(0),
      LED3(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_LED1;
      u_LED1.real = this->LED1;
      *(outbuffer + offset + 0) = (u_LED1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_LED1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_LED1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_LED1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->LED1);
      union {
        int32_t real;
        uint32_t base;
      } u_LED2;
      u_LED2.real = this->LED2;
      *(outbuffer + offset + 0) = (u_LED2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_LED2.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_LED2.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_LED2.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->LED2);
      union {
        int32_t real;
        uint32_t base;
      } u_LED3;
      u_LED3.real = this->LED3;
      *(outbuffer + offset + 0) = (u_LED3.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_LED3.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_LED3.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_LED3.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->LED3);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_LED1;
      u_LED1.base = 0;
      u_LED1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_LED1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_LED1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_LED1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->LED1 = u_LED1.real;
      offset += sizeof(this->LED1);
      union {
        int32_t real;
        uint32_t base;
      } u_LED2;
      u_LED2.base = 0;
      u_LED2.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_LED2.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_LED2.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_LED2.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->LED2 = u_LED2.real;
      offset += sizeof(this->LED2);
      union {
        int32_t real;
        uint32_t base;
      } u_LED3;
      u_LED3.base = 0;
      u_LED3.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_LED3.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_LED3.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_LED3.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->LED3 = u_LED3.real;
      offset += sizeof(this->LED3);
     return offset;
    }

    const char * getType(){ return "riki_msgs/Led"; };
    const char * getMD5(){ return "872502f8a1825d221fb8b98672c92037"; };

  };

}
#endif
