#ifndef _ROS_riki_msgs_Buzzer_h
#define _ROS_riki_msgs_Buzzer_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace riki_msgs
{

  class Buzzer : public ros::Msg
  {
    public:
      typedef int32_t _BUZZER1_type;
      _BUZZER1_type BUZZER1;

    Buzzer():
      BUZZER1(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_BUZZER1;
      u_BUZZER1.real = this->BUZZER1;
      *(outbuffer + offset + 0) = (u_BUZZER1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_BUZZER1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_BUZZER1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_BUZZER1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->BUZZER1);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_BUZZER1;
      u_BUZZER1.base = 0;
      u_BUZZER1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_BUZZER1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_BUZZER1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_BUZZER1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->BUZZER1 = u_BUZZER1.real;
      offset += sizeof(this->BUZZER1);
     return offset;
    }

    const char * getType(){ return "riki_msgs/Buzzer"; };
    const char * getMD5(){ return "961d0bf227fa8152deae5f5d68ec086e"; };

  };

}
#endif
