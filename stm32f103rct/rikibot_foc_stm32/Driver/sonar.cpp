#include "sonar.h"
#include "interrupt.h"


//GPIO_TypeDef*   SONAR_ECHO_PORT[SONARn] = {RIKI_ECHO1_GPIO_PORT, RIKI_ECHO2_GPIO_PORT, RIKI_ECHO3_GPIO_PORT, RIKI_ECHO4_GPIO_PORT}; 
//GPIO_TypeDef*   SONAR_TRIG_PORT[SONARn] = {RIKI_TRIG1_GPIO_PORT, RIKI_TRIG2_GPIO_PORT, RIKI_TRIG3_GPIO_PORT, RIKI_TRIG4_GPIO_PORT}; 
//const uint32_t  SONAR_ECHO_CLK[SONARn] = {RIKI_ECHO1_GPIO_CLK, RIKI_ECHO2_GPIO_CLK, RIKI_ECHO3_GPIO_CLK, RIKI_ECHO4_GPIO_CLK };
//const uint32_t  SONAR_TRIG_CLK[SONARn] = {RIKI_TRIG1_GPIO_CLK, RIKI_TRIG2_GPIO_CLK, RIKI_TRIG3_GPIO_CLK, RIKI_TRIG4_GPIO_CLK };
//const uint16_t  SONAR_ECHO_PIN[SONARn] = {RIKI_ECHO1_PIN, RIKI_ECHO2_PIN, RIKI_ECHO3_PIN, RIKI_ECHO4_PIN}; 
//const uint16_t  SONAR_TRIG_PIN[SONARn] = {RIKI_TRIG1_PIN, RIKI_TRIG2_PIN, RIKI_TRIG3_PIN, RIKI_TRIG4_PIN}; 
//const uint16_t  SONAR_IT[SONARn] = {TIM_IT_CC1, TIM_IT_CC2, TIM_IT_CC3, TIM_IT_CC4};
//const uint16_t  SONAR_CHANNEL[SONARn] = {TIM_Channel_1, TIM_Channel_2, TIM_Channel_3, TIM_Channel_4};

GPIO_TypeDef*   SONAR_ECHO_PORT[SONARn] = {RIKI_ECHO1_GPIO_PORT}; 
GPIO_TypeDef*   SONAR_TRIG_PORT[SONARn] = {RIKI_TRIG1_GPIO_PORT}; 
const uint32_t  SONAR_ECHO_CLK[SONARn] = {RIKI_ECHO1_GPIO_CLK};
const uint32_t  SONAR_TRIG_CLK[SONARn] = {RIKI_TRIG1_GPIO_CLK};
const uint16_t  SONAR_ECHO_PIN[SONARn] = {RIKI_ECHO1_PIN}; 
const uint16_t  SONAR_TRIG_PIN[SONARn] = {RIKI_TRIG1_PIN}; 
const uint16_t  SONAR_IT[SONARn] = {TIM_IT_CC1};
const uint16_t  SONAR_CHANNEL[SONARn] = {TIM_Channel_1};


Ultrasonic::Ultrasonic(Sonar_TypeDef _Sonar)
{
	nSonar = _Sonar;
	Sonar = this;
	state = 0;
}

//超声波初始化
void Ultrasonic::begin()
{
  GPIO_InitTypeDef GPIO_InitStructure;                  
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;        
  TIM_ICInitTypeDef TIM_ICInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RIKI_SONAR_TIM_CLK, ENABLE);     //使能TIM1时钟	

	RCC_APB2PeriphClockCmd(SONAR_ECHO_CLK[this->nSonar], ENABLE); 
	RCC_APB2PeriphClockCmd(SONAR_TRIG_CLK[this->nSonar], ENABLE); 
		
	GPIO_InitStructure.GPIO_Pin = SONAR_TRIG_PIN[this->nSonar];              
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;          
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;         
	GPIO_Init(SONAR_TRIG_PORT[this->nSonar], &GPIO_InitStructure);      
		
	GPIO_InitStructure.GPIO_Pin = SONAR_ECHO_PIN[this->nSonar] ;    
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;                   
	GPIO_Init(SONAR_ECHO_PORT[this->nSonar], &GPIO_InitStructure);    
	GPIO_ResetBits(SONAR_ECHO_PORT[this->nSonar], SONAR_ECHO_PIN[this->nSonar]);   
        
	TIM_TimeBaseStructure.TIM_Period = (0XFFFF-1);                    //设定计数器自动重装值 
	TIM_TimeBaseStructure.TIM_Prescaler = (72-1);                     //预分频器   
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;         //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;     //TIM向上计数模式
	TIM_TimeBaseInit(RIKI_SONAR_TIM, &TIM_TimeBaseStructure);       //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位                    
    

	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;    //上升沿捕获
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;//映射到TI1上
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	        //配置输入分频,不分频       
	TIM_ICInitStructure.TIM_ICFilter = 0x00;                       //IC1F=0000 配置输入滤波器 不滤波
	
	TIM_ICInitStructure.TIM_Channel = SONAR_CHANNEL[this->nSonar];               //CC1S=01 	选择输入端 IC1映射到TI1上    
	TIM_ICInit(RIKI_SONAR_TIM, &TIM_ICInitStructure); 
	TIM_ITConfig(RIKI_SONAR_TIM, SONAR_IT[this->nSonar], ENABLE);                //允许更新中断 ,允许CC1IE捕获中断	
	
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = RIKI_SONAR_TIM_IRQ;        //TIM2中断      
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;       //先占优先级3级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;              //从优先级0级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                 //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);                                 //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器  

	TIM_Cmd(RIKI_SONAR_TIM, ENABLE ); 	                            //使能定时器2
}
 
float Ultrasonic::distance()
{
    GPIO_SetBits(SONAR_TRIG_PORT[this->nSonar], SONAR_TRIG_PIN[this->nSonar]);  
    delay_us(20);		                      
    GPIO_ResetBits(SONAR_TRIG_PORT[this->nSonar], SONAR_TRIG_PIN[this->nSonar]);
	
		return dis_result;
}


void Ultrasonic::irq(void)
{	
	if((state & 0X80)==0){  //还未成功捕获
		if(TIM_GetITStatus(RIKI_SONAR_TIM,  SONAR_IT[this->nSonar]) != RESET){   //捕获1发生捕获事件
			TIM_ClearITPendingBit(RIKI_SONAR_TIM,  SONAR_IT[this->nSonar]);	    //清除中断标志位
			if(state & 0X40){   //捕获到一个上升沿
				downval = TIM_GetCapture1(RIKI_SONAR_TIM);  //记录下此时的定时器计数值
				TIM_OC1PolarityConfig(RIKI_SONAR_TIM, TIM_ICPolarity_Rising);
				if(downval <  upval) timeout = 65535;      //高电平太长了
				else  timeout = 0;
				temp = downval - upval + timeout;//得到总的高电平的时间      
				dis_result = temp / 58.0;		
				state=0;  //捕获标志位清零
			}else{
				state |= 0X40;   //标记捕获到了上升沿
				upval = TIM_GetCapture1(RIKI_SONAR_TIM);//获取上升沿数据
				TIM_OC1PolarityConfig(RIKI_SONAR_TIM, TIM_ICPolarity_Falling);    //设置为下降沿捕获
			}
			TIM_ClearITPendingBit(RIKI_SONAR_TIM, SONAR_IT[nSonar]);
		}			     	    					   
	}
}
