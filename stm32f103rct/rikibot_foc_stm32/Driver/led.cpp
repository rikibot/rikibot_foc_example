#include "led.h"
//LED灯初始化
void Led::init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RIKI_LED_GPIO_CLK, ENABLE);  //使能LED 控制管脚的GPIO口时钟

	GPIO_InitStructure.GPIO_Pin     = RIKI_LED_PIN;     //初始化LED 控制管脚 RIKI_LED_PIN-->PC3
	GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStructure.GPIO_Speed   = GPIO_Speed_50MHz; //IO口速度为50MHz
	GPIO_Init(RIKI_LED_GPIO_PORT, &GPIO_InitStructure); //根据设定参数初始化RIKI_LED_GPIO_PORT-->PC3
}

void Led::on_off(bool status)   //LED灯亮灭控制
{
	if(status == true){
		GPIO_SetBits(RIKI_LED_GPIO_PORT, RIKI_LED_PIN);  //LED GPIO口输出高电平
	}else{
		GPIO_ResetBits(RIKI_LED_GPIO_PORT, RIKI_LED_PIN);//LED GPIO口输出低电平
	}
}
