//#ifdef __cplusplus
//extern "C" {
//#endif
#ifndef _RC_H_
#define _RC_H_

#include "config.h"

class RcRemote {
public:
	RcRemote();
	~RcRemote(){}
	void begin();
	uint16_t get_ch1value();
	uint16_t get_ch2value();
	uint16_t get_ch3value();
	uint16_t get_ch4value();
	void irq();
protected:
	uint8_t  state[4]; 
	uint16_t val[4]; 
	uint16_t upval[4];
	uint16_t downval[4];
	uint32_t timeout[4];
	long long temp[4];
};



#endif //_RC_H_

//#ifdef __cplusplus
//}
//#endif
