#include "motor.h"

GPIO_TypeDef* MOTOR_PORT[MOTORn] = {RIKI_MOTOR1_GPIO_PORT, RIKI_MOTOR2_GPIO_PORT, RIKI_MOTOR3_GPIO_PORT, RIKI_MOTOR4_GPIO_PORT};
GPIO_TypeDef* MOTOR_PWM_PORT[MOTORn] = {RIKI_MOTOR1_PWM_PORT, RIKI_MOTOR2_PWM_PORT, RIKI_MOTOR3_PWM_PORT, RIKI_MOTOR4_PWM_PORT};
TIM_TypeDef*  MOTOR_PWM_TIM[MOTORn] = {RIKI_MOTOR1_PWM_TIM, RIKI_MOTOR2_PWM_TIM, RIKI_MOTOR3_PWM_TIM, RIKI_MOTOR4_PWM_TIM,};
const uint32_t  MOTOR_PORT_CLK[MOTORn] = {RIKI_MOTOR1_GPIO_CLK, RIKI_MOTOR2_GPIO_CLK, RIKI_MOTOR3_GPIO_CLK, RIKI_MOTOR4_GPIO_CLK};
const uint32_t  MOTOR_PWM_PORT_CLK[MOTORn] = {RIKI_MOTOR1_PWM_CLK, RIKI_MOTOR2_PWM_CLK, RIKI_MOTOR3_PWM_CLK, RIKI_MOTOR4_PWM_CLK,};
const uint32_t  MOTOR_PWM_TIM_CLK[MOTORn] = {RIKI_MOTOR1_PWM_TIM_CLK, RIKI_MOTOR2_PWM_TIM_CLK, RIKI_MOTOR3_PWM_TIM_CLK, RIKI_MOTOR4_PWM_TIM_CLK};
const uint16_t  MOTOR_A_PIN[MOTORn] = {RIKI_MOTOR1_A_PIN, RIKI_MOTOR2_A_PIN, RIKI_MOTOR3_A_PIN, RIKI_MOTOR4_A_PIN,};
const uint16_t  MOTOR_B_PIN[MOTORn] = {RIKI_MOTOR1_B_PIN, RIKI_MOTOR2_B_PIN, RIKI_MOTOR3_B_PIN, RIKI_MOTOR4_B_PIN};
const uint16_t  MOTOR_PWM_PIN[MOTORn] = {RIKI_MOTOR1_PWM_PIN, RIKI_MOTOR2_PWM_PIN, RIKI_MOTOR3_PWM_PIN, RIKI_MOTOR4_PWM_PIN,};


Motor::Motor(Motor_TypeDef _motor, uint32_t _arr, uint32_t _psc)
{
	motor = _motor;
	arr = _arr;
	psc = _psc;
}
//电机初始化
void Motor::init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(MOTOR_PORT_CLK[this->motor] | MOTOR_PWM_PORT_CLK[this->motor], ENABLE);
	 /** init motor gpio **/
	GPIO_InitStructure.GPIO_Pin     = MOTOR_A_PIN[this->motor] | MOTOR_B_PIN[this->motor];
	GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed   = GPIO_Speed_50MHz; 
	GPIO_Init(MOTOR_PORT[this->motor], &GPIO_InitStructure);

	/** init motor pwm gpio **/
	GPIO_InitStructure.GPIO_Pin     = MOTOR_PWM_PIN[this->motor];
	GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_AF_PP; //复用推挽输出
	GPIO_InitStructure.GPIO_Speed   = GPIO_Speed_50MHz;
	GPIO_Init(MOTOR_PWM_PORT[this->motor], &GPIO_InitStructure);

	motor_pwm_init();
}
//PWM控制引脚初始化
void Motor::motor_pwm_init()
{
	TIM_TimeBaseInitTypeDef TIM_BaseInitStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	RCC_APB1PeriphClockCmd(MOTOR_PWM_TIM_CLK[this->motor], ENABLE);

	TIM_BaseInitStructure.TIM_Period                = this->arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_BaseInitStructure.TIM_Prescaler             = this->psc; //设置用来作为TIMx时钟频率除数的预分频值 
	TIM_BaseInitStructure.TIM_ClockDivision         = TIM_CKD_DIV1; 
	TIM_BaseInitStructure.TIM_CounterMode           = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_BaseInitStructure.TIM_RepetitionCounter     = 0; //设置时钟分割:TDTS = Tck_tim
 
	TIM_TimeBaseInit(MOTOR_PWM_TIM[this->motor], &TIM_BaseInitStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;   //选择定时器模式:TIM脉冲宽度调制模式1
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;   //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = 0;  //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;   //输出极性:TIM输出比较极性高

	if(this->motor == MOTOR3){
		TIM_OC1Init(MOTOR_PWM_TIM[this->motor], &TIM_OCInitStructure); //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC1PreloadConfig(MOTOR_PWM_TIM[this->motor], TIM_OCPreload_Enable); //CH1预装载使能	
	}

	if(this->motor == MOTOR4) {
		TIM_OC2Init(MOTOR_PWM_TIM[this->motor], &TIM_OCInitStructure); //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC2PreloadConfig(MOTOR_PWM_TIM[this->motor], TIM_OCPreload_Enable);//CH2预装载使能	
	}

	if(this->motor == MOTOR2){
		TIM_OC3Init(MOTOR_PWM_TIM[this->motor], &TIM_OCInitStructure); //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC3PreloadConfig(MOTOR_PWM_TIM[this->motor], TIM_OCPreload_Enable);//CH3预装载使能	
	}

	if(this->motor == MOTOR1) {
		TIM_OC4Init(MOTOR_PWM_TIM[this->motor], &TIM_OCInitStructure); //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC4PreloadConfig(MOTOR_PWM_TIM[this->motor], TIM_OCPreload_Enable);//CH4预装载使能	
	}
	TIM_ARRPreloadConfig(MOTOR_PWM_TIM[this->motor], ENABLE); //使能TIMx在ARR上的预装载寄存器

	TIM_CtrlPWMOutputs(MOTOR_PWM_TIM[this->motor], ENABLE); //MOE 主输出使能	
	TIM_Cmd(MOTOR_PWM_TIM[this->motor], ENABLE);            //使能TIM1
    
    
}
//正反转控制
void Motor::spin(int pwm)
{
#if RIKIBOT_OMNI_3WD 
    if((this->motor == MOTOR1) || (this->motor == MOTOR2)){
        if(pwm > 0){
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
        }
        if(pwm < 0) {
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
        }
    }
    if(this->motor == MOTOR3){
        if(pwm < 0){
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
        }
        if(pwm > 0) {
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
        }
    }
    
#endif
    
#if RIKIBOT_2WD || RIKIBOT_4WD || RIKIBOT_ACKERMANN || RIKIBOT_TANK 
    if((this->motor == MOTOR1) || (this->motor == MOTOR4)){
        if(pwm > 0){
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
        }
        if(pwm < 0) {
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
        }
    }
    if((this->motor == MOTOR2) || (this->motor == MOTOR3)){
        
        if(pwm < 0){
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
        }
        if(pwm > 0) {
            GPIO_SetBits(MOTOR_PORT[this->motor], MOTOR_B_PIN[this->motor]);
            GPIO_ResetBits(MOTOR_PORT[this->motor], MOTOR_A_PIN[this->motor]);
        }
    }
#endif
    
	if(this->motor == MOTOR3){
		TIM_SetCompare1(MOTOR_PWM_TIM[this->motor], abs(pwm));
	}
    
	if(this->motor == MOTOR4){
		TIM_SetCompare2(MOTOR_PWM_TIM[this->motor], abs(pwm));
	}
    
	if(this->motor == MOTOR2){
		TIM_SetCompare3(MOTOR_PWM_TIM[this->motor], abs(pwm));
	}
    
	if(this->motor == MOTOR1){
		TIM_SetCompare4(MOTOR_PWM_TIM[this->motor], abs(pwm));
	}
}

//计算电机转速
void Motor::updateSpeed(long encoder_ticks) 
{
	//this function calculates the motor's RPM based on encoder ticks and delta time
	unsigned long current_time = millis();  //获取当前单位时间
	unsigned long dt = current_time - prev_update_time_; //当前时间减去-之前的时间 = 运行时间

	//convert the time from milliseconds to minutes
	double dtm = (double)dt / 60000;    //秒换成分钟
	double delta_ticks = encoder_ticks - prev_encoder_ticks_; //编码器脉冲 = 当前脉冲数 - 上次的脉冲数

	//calculate wheel's speed (RPM)
	rpm = (delta_ticks / counts_per_rev_) / dtm; //转速 = 脉冲数 / 一圈的脉冲数 1560 / 运行时间

	prev_update_time_ = current_time;   //记下这次时间 作为下次的时间
	prev_encoder_ticks_ = encoder_ticks;//记下这次的编码器脉冲数 作为下次的脉冲数
}

