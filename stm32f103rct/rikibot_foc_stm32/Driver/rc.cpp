#include "rc.h"
#include "interrupt.h"

RcRemote::RcRemote()
{
	Rc = this;
	memset(state, sizeof(state), 0);
}

void RcRemote::begin()
{
  GPIO_InitTypeDef GPIO_InitStructure;                  
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;        
  TIM_ICInitTypeDef TIM_ICInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RIKI_RC_TIM_CLK, ENABLE);     //使能TIM1时钟	
	RCC_APB2PeriphClockCmd(RIKI_RCCH_GPIO_CLK, ENABLE); 

	GPIO_InitStructure.GPIO_Pin = RIKI_RCCH1_PIN|RIKI_RCCH2_PIN|RIKI_RCCH3_PIN|RIKI_RCCH4_PIN ;    
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;                   
	GPIO_Init(RIKI_RCCH_GPIO_PORT, &GPIO_InitStructure);    
	GPIO_ResetBits(RIKI_RCCH_GPIO_PORT, RIKI_RCCH1_PIN|RIKI_RCCH2_PIN|RIKI_RCCH3_PIN|RIKI_RCCH4_PIN);   
        
	TIM_TimeBaseStructure.TIM_Period = (0XFFFF-1);                    //设定计数器自动重装值 
	TIM_TimeBaseStructure.TIM_Prescaler = (72-1);                     //预分频器   
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;         //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;     //TIM向上计数模式
	TIM_TimeBaseInit(RIKI_RC_TIM, &TIM_TimeBaseStructure);       //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位                    
    

	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;    //上升沿捕获
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;//映射到TI1上
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;	        //配置输入分频,不分频  
	TIM_ICInitStructure.TIM_ICFilter = 0x00;                       //IC1F=0000 配置输入滤波器 不滤波
	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;               //CC1S=01 	选择输入端 IC1映射到TI1上    
	TIM_ICInit(RIKI_RC_TIM, &TIM_ICInitStructure); 
	TIM_ITConfig(RIKI_RC_TIM,  TIM_IT_CC1, ENABLE);                //允许更新中断 ,允许CC1IE捕获中断	
	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;               //CC1S=01 	选择输入端 IC1映射到TI1上    
	TIM_ICInit(RIKI_RC_TIM, &TIM_ICInitStructure); 
	TIM_ITConfig(RIKI_RC_TIM,  TIM_IT_CC2, ENABLE);                //允许更新中断 ,允许CC1IE捕获中断	
	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_3;               //CC1S=01 	选择输入端 IC1映射到TI1上    
	TIM_ICInit(RIKI_RC_TIM, &TIM_ICInitStructure); 
	TIM_ITConfig(RIKI_RC_TIM,  TIM_IT_CC3, ENABLE);                //允许更新中断 ,允许CC1IE捕获中断	
	
	
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_4;               //CC1S=01 	选择输入端 IC1映射到TI1上    
	TIM_ICInit(RIKI_RC_TIM, &TIM_ICInitStructure); 
	TIM_ITConfig(RIKI_RC_TIM,  TIM_IT_CC4, ENABLE);                //允许更新中断 ,允许CC1IE捕获中断	
	
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = RIKI_RC_TIM_IRQ;        //TIM2中断      
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;       //先占优先级3级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;              //从优先级0级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                 //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);                                 //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器  

	TIM_Cmd(RIKI_RC_TIM, ENABLE ); 	                            //使能定时器2
}

uint16_t RcRemote::get_ch1value()
{
	return temp[3];
}

uint16_t RcRemote::get_ch2value()
{
	return temp[2];
}

uint16_t RcRemote::get_ch3value()
{
	return temp[1];
}

uint16_t RcRemote::get_ch4value()
{
	return temp[0];
}

void RcRemote::irq(void)
{	
	if((state[0] & 0X80)==0){  //还未成功捕获
		if(TIM_GetITStatus(RIKI_RC_TIM,  TIM_IT_CC1) != RESET){   //捕获1发生捕获事件
			TIM_ClearITPendingBit(RIKI_RC_TIM,  TIM_IT_CC1);	    //清除中断标志位
			if(state[0] & 0X40){   //捕获到一个上升沿
				downval[0] = TIM_GetCapture1(RIKI_RC_TIM);  //记录下此时的定时器计数值
				TIM_OC1PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Rising);
				if(downval[0] <  upval[0]) timeout[0] = 65535;      //高电平太长了
				else  timeout[0] = 0;
				temp[0] = downval[0] - upval[0] + timeout[0];//得到总的高电平的时间      
				state[0]=0;  //捕获标志位清零
			}else{
				state[0] |= 0X40;   //标记捕获到了上升沿
				upval[0] = TIM_GetCapture1(RIKI_RC_TIM);//获取上升沿数据
				TIM_OC1PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Falling);    //设置为下降沿捕获	
			}
			TIM_ClearITPendingBit(RIKI_RC_TIM, TIM_IT_CC1);
		}			     	    					   
	}
	
	if((state[1] & 0X80)==0){  //还未成功捕获
		if(TIM_GetITStatus(RIKI_RC_TIM,  TIM_IT_CC2) != RESET){   //捕获1发生捕获事件
			TIM_ClearITPendingBit(RIKI_RC_TIM,  TIM_IT_CC2);	    //清除中断标志位
			if(state[1] & 0X40){   //捕获到一个上升沿
				downval[1] = TIM_GetCapture2(RIKI_RC_TIM);  //记录下此时的定时器计数值
				TIM_OC2PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Rising);
				if(downval[1] <  upval[1]) timeout[1] = 65535;      //高电平太长了
				else  timeout[1] = 0;
				temp[1] = downval[1] - upval[1] + timeout[1];//得到总的高电平的时间      
				state[1]=0;  //捕获标志位清零
			}else{
				state[1] |= 0X40;   //标记捕获到了上升沿
				upval[1] = TIM_GetCapture2(RIKI_RC_TIM);//获取上升沿数据
				TIM_OC2PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Falling);    //设置为下降沿捕获	
			}
			TIM_ClearITPendingBit(RIKI_RC_TIM, TIM_IT_CC2);
		}			     	    					   
	}
	
	if((state[2] & 0X80)==0){  //还未成功捕获
		if(TIM_GetITStatus(RIKI_RC_TIM,  TIM_IT_CC3) != RESET){   //捕获1发生捕获事件
			TIM_ClearITPendingBit(RIKI_RC_TIM,  TIM_IT_CC3);	    //清除中断标志位
			if(state[2] & 0X40){   //捕获到一个上升沿
				downval[2] = TIM_GetCapture3(RIKI_RC_TIM);  //记录下此时的定时器计数值
				TIM_OC3PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Rising);
				if(downval[2] <  upval[2]) timeout[2] = 65535;      //高电平太长了
				else  timeout[2] = 0;
				temp[2] = downval[2] - upval[2] + timeout[2];//得到总的高电平的时间      
				state[2]=0;  //捕获标志位清零
			}else{
				state[2] |= 0X40;   //标记捕获到了上升沿
				upval[2] = TIM_GetCapture3(RIKI_RC_TIM);//获取上升沿数据
				TIM_OC3PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Falling);    //设置为下降沿捕获	
			}
			TIM_ClearITPendingBit(RIKI_RC_TIM, TIM_IT_CC3);
		}			     	    					   
	}
	
	if((state[3] & 0X80)==0){  //还未成功捕获
		if(TIM_GetITStatus(RIKI_RC_TIM,  TIM_IT_CC4) != RESET){   //捕获1发生捕获事件
			TIM_ClearITPendingBit(RIKI_RC_TIM,  TIM_IT_CC4);	    //清除中断标志位
			if(state[3] & 0X40){   //捕获到一个上升沿
				downval[3] = TIM_GetCapture4(RIKI_RC_TIM);  //记录下此时的定时器计数值
				TIM_OC4PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Rising);
				if(downval[3] <  upval[3]) timeout[3] = 65535;      //高电平太长了
				else  timeout[3] = 0;
				temp[3] = downval[3] - upval[3] + timeout[3];//得到总的高电平的时间      
				state[3]=0;  //捕获标志位清零
			}else{
				state[3] |= 0X40;   //标记捕获到了上升沿
				upval[3] = TIM_GetCapture4(RIKI_RC_TIM);//获取上升沿数据
				TIM_OC4PolarityConfig(RIKI_RC_TIM, TIM_ICPolarity_Falling);    //设置为下降沿捕获	
			}
			TIM_ClearITPendingBit(RIKI_RC_TIM, TIM_IT_CC4);
		}			     	    					   
	}
}
