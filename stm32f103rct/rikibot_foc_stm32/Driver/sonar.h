#ifndef _SONAR_H_
#define _SONAR_H_

#include "config.h"

class Ultrasonic {
public:
	Ultrasonic(Sonar_TypeDef _Sonar=SONAR1);
	~Ultrasonic(){}
	void begin();
	float distance();
	void irq();
protected:
	Sonar_TypeDef nSonar;
	uint8_t  state; 
	uint16_t val; 
	uint16_t upval;
	uint16_t downval;
	uint32_t timeout;
	long long temp;
	float dis_result;
};

#endif //_SONAR_H_


