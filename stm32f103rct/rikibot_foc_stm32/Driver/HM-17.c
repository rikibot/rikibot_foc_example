#ifdef __cplusplus
extern "C" {
#endif
    
#include "HM-17.h"
#include "delay.h"
//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 

}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
int _sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{      
	while((USART3->SR&0X40)==0);//循环发送,直到发送完毕   
    USART3->DR = (u8) ch;      
	return ch;
}
#endif 

int retry=8,t;  		 
u8 temp_=1;
int len,usart_debug_time,printf_flag,ble_contorl_time;
u8 HM_17_ANGLE_X = 0,HM_17_ANGLE_Y = 0;
int bluetooth_connect_stats = 5;

//串口1中断服务程序
//注意,读取USARTx->SR能避免莫名其妙的错误   	
u8 USART_RX_BUF[USART_REC_LEN];     //接收缓冲,最大USART_REC_LEN个字节.
u8 HM17_RECV_BUFFER[USART_REC_LEN];
//接收状态
//bit15，	接收完成标志
//bit14，	接收到0x0d
//bit13~0，	接收到的有效字节数目
u16 USART_RX_STA=0; //接收状态标记	  
  
void HM_17_init(u32 bound){
    //GPIO端口设置
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);   //GPIOA时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);   //使能USART3
    
    //USART3_TX   GPIOB.10
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;              //PB.10
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;         //复用推挽输出
    GPIO_Init(GPIOB, &GPIO_InitStructure);                  //初始化GPIOB.10

    //USART3_RX	  GPIOB.11
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;              //PB11
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;   //浮空输入
    GPIO_Init(GPIOB, &GPIO_InitStructure);                  //初始化GPIOB.11  

    //Usart1 NVIC 配置
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2 ;//抢占优先级2
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//子优先级3
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
    NVIC_Init(&NVIC_InitStructure);	                        //根据指定的参数初始化VIC寄存器

    //USART 初始化设置
    USART_InitStructure.USART_BaudRate = bound;             //串口波特率
    USART_InitStructure.USART_WordLength = USART_WordLength_8b; //字长为8位数据格式
    USART_InitStructure.USART_StopBits = USART_StopBits_1;  //一个停止位
    USART_InitStructure.USART_Parity = USART_Parity_No;     //无奇偶校验位
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; //收发模式

    USART_Init(USART3, &USART_InitStructure);               //初始化串口3
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);          //开启串口接受中断
    USART_Cmd(USART3, ENABLE);                              //使能串口3 

}

void USART3_IRQHandler(void){               //串口2中断服务程序
	u8 Res;
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET){      //接收中断(接收到的数据必须是0x0d 0x0a结尾)
		Res = USART_ReceiveData(USART3);    //读取接收到的数据
		if((USART_RX_STA&0x8000)==0){       //接收未完成
        
			if(USART_RX_STA&0x4000){        //接收到了0x0d
				if(Res!=0x0a)USART_RX_STA=0;//接收错误,重新开始
				else USART_RX_STA|=0x8000;	//接收完成了 
            }else{                          //还没收到0X0D
				if(Res==0x0d)USART_RX_STA|=0x4000;
				else{
					USART_RX_BUF[USART_RX_STA&0X3FFF]=Res;
					USART_RX_STA++;
					if(USART_RX_STA>(USART_REC_LEN-1))USART_RX_STA=0;//接收数据错误,重新开始接收	  
                }		 
            }
        }   		 
    } 
} 
 


u8 HM_17_CONNECT(void)
{
    printf("AT+START\r\n"); //向蓝牙发送开始指令 接收返回数据OK+START  OK+CONN
    while(retry--){
        for(t=0;t<200;t++)  //最长等待200ms,来接收HM模块的回应
        {
            if(USART_RX_STA&0X8000)break;
            delay_ms(1);
        }	
        if(USART_RX_STA&0x8000)         //接收完一次数据
        {					   
            len=USART_RX_STA&0x3fff;    //得到此次接收到的数据长度
            if((strcmp((const char*)USART_RX_BUF,"OK+START")==0)){
                //serial.print("%s\r\n", USART_RX_BUF );
                memset(USART_RX_BUF,0,USART_REC_LEN);
                printf_flag = 3;
            }
            if( printf_flag ==3 &&(strcmp((const char*)USART_RX_BUF,"OK+CONN")==0)){
                //serial.print("%s\r\n", USART_RX_BUF );
                memset(USART_RX_BUF,0,USART_REC_LEN);
                printf_flag = 2;
            }
            if(printf_flag == 2 && USART_RX_BUF[0] == 'X'){     //判断数据流是否正确
                printf_flag = 1;
                temp_ = 0;
                retry = 8;
                break;
            }
            USART_RX_STA=0;
        }
    }
	if(retry==0)temp_=1;//失败.
    retry = 8;
    return temp_;
}


u8 HM_17_LOST(void){
    printf("AT\r\n"); //向蓝牙发送开始指令  OK+START  OK+CONN
    while(retry--){
        for(t=0;t<200;t++)  //最长等待200ms,来接收HM模块的回应
        {
            if(USART_RX_STA&0X8000)break;
            delay_ms(1);
        }	
        if(USART_RX_STA&0x8000)         //接收完一次数据
        {					   
            memset(USART_RX_BUF,0,USART_REC_LEN);
            USART_RX_STA=0;
            retry = 8;
            temp_ = 0;
            break;
        }
    }
	if(retry==0)temp_=1;//失败.
    retry = 8;
    return temp_;
}


void Get_HM17_MPU_Angle_Data(void){
    if(HM17_RECV_BUFFER[4]=='Y'){ 
        HM_17_ANGLE_X = ((HM17_RECV_BUFFER[1]-'0') *10  + (HM17_RECV_BUFFER[2]-'0'));
        if( HM17_RECV_BUFFER[7]<'0'&& HM17_RECV_BUFFER[7]>'9'){
            HM_17_ANGLE_Y = ((HM17_RECV_BUFFER[5]-'0') *10  + (HM17_RECV_BUFFER[6]-'0'));
        }
        else if(HM17_RECV_BUFFER[7]>='0'&& HM17_RECV_BUFFER[7]<='9'){
            HM_17_ANGLE_Y = ((HM17_RECV_BUFFER[5]-'0') *100  + (HM17_RECV_BUFFER[6]-'0') *10 + (HM17_RECV_BUFFER[7]-'0'));
        }
    }
    else if(HM17_RECV_BUFFER[5]=='Y'){
        HM_17_ANGLE_X = ((HM17_RECV_BUFFER[1]-'0') * 100  + (HM17_RECV_BUFFER[2]-'0') *10 + (HM17_RECV_BUFFER[3]-'0'));
        if( HM17_RECV_BUFFER[8]>='0'&& HM17_RECV_BUFFER[8]<='9' ){
            HM_17_ANGLE_Y = ((HM17_RECV_BUFFER[6]-'0') *100  + (HM17_RECV_BUFFER[7]-'0') *10 + (HM17_RECV_BUFFER[8]-'0'));
        }
        else {
            HM_17_ANGLE_Y = ((HM17_RECV_BUFFER[6]-'0') *10  + (HM17_RECV_BUFFER[7]-'0'));
        }
    }
}
 
void Determine_Connection_Status(void){
    if(bluetooth_connect_stats == 1){
        if(HM_17_CONNECT() == 0){
            bluetooth_connect_stats = 3;
            nh_debug(bluetooth_connect_stats);
            //nh.loginfo("Bluetooth is connected.\r\n");
        }else{
            bluetooth_connect_stats = 5;
            nh_debug(bluetooth_connect_stats);
            //nh.logwarn("Bluetooth Connect Failed!\r\n");
        }
    }
    if(bluetooth_connect_stats == 0){
        if(HM_17_LOST() == 0){
            bluetooth_connect_stats = 4;
            nh_debug(bluetooth_connect_stats);
            //nh.loginfo("Bluetooth disconnect.\r\n");
        }else{
            bluetooth_connect_stats = 6;
            nh_debug(bluetooth_connect_stats);
            //nh.logwarn("Bluetooth Disconnect Failed!\r\n");
        }
    }
    
    if(USART_RX_STA&0x8000){    
        if(printf_flag==1 && bluetooth_connect_stats == 3){
            strcpy((char *)HM17_RECV_BUFFER,(char *)USART_RX_BUF);
            Get_HM17_MPU_Angle_Data();
            memset(USART_RX_BUF,0,USART_REC_LEN);
            memset(HM17_RECV_BUFFER,0,USART_REC_LEN);
        }
        USART_RX_STA=0;
    }
    
    if(bluetooth_connect_stats == 4 || bluetooth_connect_stats == 5){
        HM_17_ANGLE_X = 0;
        HM_17_ANGLE_Y = 0;
    }
}




#ifdef __cplusplus
}
#endif
