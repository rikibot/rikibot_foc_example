# 一、RIKIBOT FOC控制板介绍（支持ROS1、ROS2）

RIKIBOT-FOC无刷轮毂电机驱动板，是一款兼容市面上大数据3相8线500W以内霍尔轮毂电机，宽电压驱动：24-36V，最大电流15A，系统默认电机极对数为15，控制简单通过串口即可控制轮毂电机，轮毂电机的优势很明显，工作安静、扭力大、平稳、速度与负载可以兼得，一改有刷电机有速度没负载，有负载没速度的尴尬情况，因此可广泛应用于ROS移动机器人、小型AGV、服务机器人领域。

![Image text](https://gitee.com/rikibot/rikibot_foc_example/raw/master/image/2.png)

# 二、RIKIBOT-FOC接口介绍

1、黄绿蓝三相电机线，为UVW三相电机线

2、电源接口：市面上常用的24-36V锂电池T型接口

3、开机按钮：为触发式自复位按钮信号，也就是按一下按钮可复位按钮即可，切记不要接自锁式按钮

4、左右侧电机霍尔线：从上图看，从左到右的接口顺序为GND-绿-蓝-黄-5V信号，每家电机对UVW线色可能不一样，所以在这里接线时，绿-蓝-黄不一定与板上一样，这个自己尝试后才能确认下来，如果接上去控制异常，自己需要调整这三个的线序

5、串口控制线：从上图看，从左到右的接口顺序为开机信号-TX(数据发送IO)-RX(数据接收IO)-GND，这里的开机信号与上面的开机按钮一样，如果不接开机按钮只需要给一个1秒左右高电平信号即可。都可以实现驱动板的开机。

![Image text](https://gitee.com/rikibot/rikibot_foc_example/raw/master/image/1.png)

# 三、控制协议

这里以stm32控制板为例，发送数据一个结构体与接收数据一个结构体，按下面的格式进行数据填充即可

```
typedef struct{                //接收数据结构体
    uint16_t  start;          //帧ID --0xABCD
    int16_t   rpmR;    				//右轮转速
    int16_t   rpmL;    				//左轮转速
    int16_t   batVoltage;     //驱动板电压
    int16_t   boardTemp;      //驱动板温度
    int16_t 	curL_DC;        //左轮电流
    int16_t 	curR_DC;        //右轮电流
    uint16_t  checksum;       //校验和
} RikibotFeedback;

typedef struct{           //发送结构体
   uint16_t start;        //帧ID 0xABCD
   int16_t  mSpeedR;      //右轮速度（驱动板速度映射为0-1000，然后转速为0-366，也就是0-366RPM通过map映射成0-1000的PWM值）
   int16_t  mSpeedL;      //左轮速度
   uint16_t checksum;     //校验和
} RikibotCommand;
```

接收校验和的计算为接收结构体内数据进行异或运算：

```
checksum = (uint16_t)(FrontFeedback.start ^ FrontFeedback.rpmR ^ FrontFeedback.rpmL
			^ FrontFeedback.batVoltage ^ FrontFeedback.boardTemp 
		      ^ FrontFeedback.curL_DC ^ FrontFeedback.curR_DC);
```



发送数据校验和的计算为发送结构体内的数据进行异或运算：

```
FrontCommand.checksum 	= (uint16_t)(FrontCommand.start ^ FrontCommand.mSpeedR ^ FrontCommand.mSpeedL);
```



开机信号，这里直接给一个1秒左右的高电平即可，关于开机信号，像树莓派或者jetson nano上的GPIO驱动电流过小不可以直接控制，需要通过信号隔离进行控制。

```
GPIO_SetBits(RIKI_MOTOR_EN1_GPIO_PORT, RIKI_MOTOR_EN1_PIN); 	
delay(1000);
GPIO_ResetBits(RIKI_MOTOR_EN1_GPIO_PORT, RIKI_MOTOR_EN1_PIN);
	
```

# 四、工程示例

里面工程提供了arduino、stm32、ros1、ros2、liunx等相关示例，相关的用户可以根据自己需要构建相关的环境，构建时请参考示例的README文件来构建

------------
QQ交流群：130486387

[RIKIBOT店铺连接](https://shop472519213.taobao.com/?spm=2013.1.1000126.d21.749c789bHcH1O6)

